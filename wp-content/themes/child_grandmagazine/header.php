<?php
/**
 * The Header for the template.
 *
 * @package WordPress
 */
 
if (!isset( $content_width ) ) $content_width = 1440;

/*if(session_id() == '') {
	session_start();
}*/
 
$grandmagazine_homepage_style = grandmagazine_get_homepage_style();

?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php if(isset($grandmagazine_homepage_style) && !empty($grandmagazine_homepage_style)) { echo 'data-style="'.esc_attr($grandmagazine_homepage_style).'"'; } ?>>
<head>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-GPHV8PX95F"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-GPHV8PX95F');
</script>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,500,600,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=PT+Serif:400,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@900&family=Work+Sans:wght@400;500;600;700;900&display=swap" rel="stylesheet">
<meta property="fb:pages" content="102886746417877" />
<meta property="fb:app_id" content="241414607171864" />
<?php
$type = "";
	if (is_front_page()) {
		$type = "home";
	} else if(is_page()){
		$type = "page";
	}

	//Fallback compatibility for favicon
	if(!function_exists( 'has_site_icon' ) || ! has_site_icon() ) 
	{
		/**
		*	Get favicon URL
		**/
		$tg_favicon = kirki_get_option('tg_favicon');
		
		if(!empty($tg_favicon))
		{
?>
			<link rel="shortcut icon" href="<?php echo esc_url($tg_favicon); ?>" />
<?php
		}
	}
?> 
<?php if(!is_404()){ ?>
<script async src="//js-sec.indexww.com/ht/p/189664-17010472658127.js"></script>
<script async src="https://securepubads.g.doubleclick.net/tag/js/gpt.js"></script>
<?php } ?>
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();
	$type_content = get_post_meta($post->ID, 'type_content_pg', true);
?>
<!-- Google Analytics -->
<script>
jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();
</script>
</head>

<body <?php body_class($type); ?>>

	<?php
		//Check if disable right click
		$tg_enable_right_click = kirki_get_option('tg_enable_right_click');
		
		//Check if disable image dragging
		$tg_enable_dragging = kirki_get_option('tg_enable_dragging');
		
		//Check if sticky menu
		$tg_fixed_menu = kirki_get_option('tg_fixed_menu');
		
		//Check if smart sticky menu
		$tg_smart_fixed_menu = kirki_get_option('tg_smart_fixed_menu');
		
		//Check if sticky sidebar
		$tg_sidebar_sticky = kirki_get_option('tg_sidebar_sticky');
		
		//Check if display top bar
		$tg_topbar = kirki_get_option('tg_topbar');
		if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['topbar']) && !empty($_GET['topbar']))
		{
			$tg_topbar = true;
		}
		
		//Check slider layout
		$tg_blog_slider_layout = kirki_get_option('tg_blog_slider_layout');
		if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['layout']) && ($_GET['layout'] == 'fullwidth' OR $_GET['layout'] == 'full_fullwidth'))
		{
			$tg_blog_slider_layout = '3cols-slider';
		}
	?>
	<input type="hidden" id="pp_enable_right_click" name="pp_enable_right_click" value="<?php echo esc_attr($tg_enable_right_click); ?>"/>
	<input type="hidden" id="pp_enable_dragging" name="pp_enable_dragging" value="<?php echo esc_attr($tg_enable_dragging); ?>"/>
	<input type="hidden" id="pp_image_path" name="pp_image_path" value="<?php echo get_template_directory_uri(); ?>/images/"/>
	<input type="hidden" id="pp_homepage_url" name="pp_homepage_url" value="<?php echo esc_url(home_url('/')); ?>"/>
	<input type="hidden" id="pp_ajax_search" name="pp_ajax_search" value="1"/>
	<input type="hidden" id="pp_fixed_menu" name="pp_fixed_menu" value="<?php echo esc_attr($tg_fixed_menu); ?>"/>
	<input type="hidden" id="tg_smart_fixed_menu" name="tg_smart_fixed_menu" value="<?php echo esc_attr($tg_smart_fixed_menu); ?>"/>
	<input type="hidden" id="pp_topbar" name="pp_topbar" value="<?php echo esc_attr($tg_topbar); ?>"/>
	<input type="hidden" id="tg_blog_slider_layout" name="tg_blog_slider_layout" value="<?php echo esc_attr($tg_blog_slider_layout); ?>"/>
	<input type="hidden" id="pp_back" name="pp_back" value="<?php esc_html_e( 'Back', 'grandmagazine' ); ?>"/>
	<input type="hidden" id="tg_sidebar_sticky" name="tg_sidebar_sticky" value="<?php echo esc_attr($tg_sidebar_sticky); ?>"/>
	
	<?php
		//Check footer sidebar columns
		$tg_footer_sidebar = kirki_get_option('tg_footer_sidebar');
	?>
	<input type="hidden" id="pp_footer_style" name="pp_footer_style" value="<?php echo esc_attr($tg_footer_sidebar); ?>"/>
	
	<!-- Begin mobile menu -->
	<div class="mobile_menu_wrapper"> 
		<a id="close_mobile_menu" href="javascript:;"><i class="fa fa-close"></i></a>   	
	    <?php 
	    	//Check if has custom menu
			if(is_object($post) && $post->post_type == 'page')
			{
			    $page_menu = get_post_meta($post->ID, 'page_menu', true);
			}	
			
			if ( has_nav_menu( 'side-menu' ) ) 
			{
			    //Get page nav
			    wp_nav_menu( 
			        array( 
			            'menu_id'			=> 'mobile_main_menu',
		                'menu_class'		=> 'mobile_main_nav',
			            'theme_location' 	=> 'side-menu',
			        )
			    ); 
			}
		?>
		
		<!-- Begin side menu sidebar -->
		<div class="page_content_wrapper">
			<div class="sidebar_wrapper">
		        <div class="sidebar">
		        
		        	<div class="content">
		        
		        		<ul class="sidebar_widget">
		        		<?php dynamic_sidebar('Side Menu Sidebar'); ?>
		        		</ul>
		        	
		        	</div>
		    
		        </div>
			</div>
		</div>
		<!-- End side menu sidebar -->
	</div>
	<!-- End mobile menu -->

	<!-- Begin template wrapper -->
	<?php
		//Check if front page and have featured category set
		$wrapper_class = '';
		$tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
		if(!empty($tg_blog_featured_cat) && is_front_page() && $paged <= 1)
		{
			$wrapper_class = 'menu_transparent';
		}
		
		//Check if category page
		if(is_category() && function_exists('z_taxonomy_image_url'))
		{
			$pp_page_bg = z_taxonomy_image_url();
			
			if(!empty($pp_page_bg))
			{
				$wrapper_class = 'menu_transparent';
			}
		}
		
		//Check if tag page
		if(is_tag() && function_exists('z_taxonomy_image_url'))
		{
			$pp_page_bg = z_taxonomy_image_url();
			
			if(!empty($pp_page_bg))
			{
				$wrapper_class = 'menu_transparent';
			}
		}
		
		$page_menu_transparent = '';
		
		if(is_page() && has_post_thumbnail($post->ID, 'original'))
		{
			$wrapper_class = 'menu_transparent';
		}
		
		$shop_page_id = get_option('woocommerce_shop_page_id');
		if(grandmagazine_is_woocommerce_page() && has_post_thumbnail($shop_page_id, 'original'))
		{
			$wrapper_class = 'menu_transparent';
		}
		
		//Check if single page
		if(is_single() && $post->post_type == 'post')
		{
			$post_layout = get_post_meta($post->ID, 'post_layout', true);
			$static_single_layout_arr = array(
				'single-post-r-title-parallax', 
				'single-post-l-title-parallax',
				'single-post-f-title-parallax',
				'single-post-r-minimal',
				'single-post-l-minimal',
				'single-post-f-minimal',
			);
			
			if(has_post_thumbnail($post->ID, 'original') && !in_array($post_layout, $static_single_layout_arr))
			{
				$wrapper_class = 'menu_transparent';
			}
		}
	?>
	<div class="header_ads">
		<div class="ad-160x600 left" id="ad_spain_sky_left">
			<div id="pg_sky_izq"></div>
		</div>
		<div class="ad-1024x90" id="ad_spain_top">
			<div id="pg_megabanner"></div>
		</div>
		<div class="ad-160x600 right" id="ad_spain_sky_right">
			<div id="pg_sky_der"></div>
		</div>
	</div>
	<div id="wrapper" class="<?php echo esc_attr($wrapper_class); ?>">
	
	<?php
	    //Get main menu layout
		get_template_part("/templates/template-topmenu");
	?>
