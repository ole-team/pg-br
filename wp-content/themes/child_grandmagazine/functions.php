<?php
 use Facebook\InstantArticles\Elements\Ad;
add_action( 'wp_enqueue_scripts', 'theme_slug_enqueue_styles' );
function theme_slug_enqueue_styles() {
 
    $parent_style = 'grandmagazine-style';
 
    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

/* Funciones personalizadas */
require_once( get_stylesheet_directory() . '/lib/customchild.lib.php');
require_once( get_stylesheet_directory() . '/lib/widgets.lib.php');

add_image_size('shows', 350, 150, true);
add_image_size('videos', 350, 350, true);
add_image_size('mobile-ft', 650, 650, true);

function grandmagazine_enqueue_front_page_scripts_custom_accordion() {
		wp_enqueue_script("accordion_play", get_stylesheet_directory_uri()."/js/accordion.js", false, GRANDMAGAZINE_THEMEVERSION, true);
	}
add_action( 'wp_enqueue_scripts', 'grandmagazine_enqueue_front_page_scripts_custom_accordion' );

function grandmagazine_get_post_blog_newspaper() {
	$paged = 0;
	if(isset($_POST['paged']))
	{
	    $paged = $_POST['paged'];
	}
	
	$view = 'latest';
	if(isset($_POST['view']))
	{
	    $view = $_POST['view'];
	}
	
	$query_arr = grandmagazine_get_posts_by_view($view, $paged);
	//grandmagazine_debug($query_arr);
	query_posts($query_arr);
	
	$wp_query = grandmagazine_get_wp_query();
    $count_all_posts = $wp_query->post_count;
	
	$key = 0;
	
	if (have_posts()) : while (have_posts()) : the_post();
		$key++;
		$image_thumb = '';
		$image_id = get_post_thumbnail_id(get_the_ID());
		$post_categories = get_the_category($post->ID);
?>
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="post_wrapper">
	    <div class="post_content_wrapper">
	    	<div class="post_header featured_posts_filter mixed tabsHome">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0])) {
				        $post_content_class = 'two_third last';
				?>
				<div class="post_img static">
			      	<a href="<?php the_permalink(); ?>">
			      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
			      	</a>
			    </div>
				<?php } ?>	
				<div class="post_header_title">
					<?php  if(!empty($post_categories)) { ?>
					<div class="post_info_cat_inverse">
					    <?php
					    $primaryCat = get_post_primary_category($post->ID,'category');
					    	/*$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );*/
					    ?>
					        <a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
					    <?php //} ?>
					</div>
					<?php } ?>
			      	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true); if(!empty($custom_style)){ echo $custom_style;}else{the_title();} ?></a></h2>
			   </div>
			</div>
	    </div>
	</div>
</div>
<?php
	endwhile; endif;

	die();
}

/**
*	Setup AJAX search function
**/
add_action('wp_ajax_grandmagazine_ajax_search', 'grandmagazine_ajax_search');
add_action('wp_ajax_nopriv_grandmagazine_ajax_search', 'grandmagazine_ajax_search');

function grandmagazine_ajax_search() 
{
	$wpdb = grandmagazine_get_wpdb();
	
	if (strlen($_POST['s'])>0) {
		$limit=10;
		$s=strtolower(addslashes($_POST['s']));
		$querystr = "
			SELECT $wpdb->posts.*
			FROM $wpdb->posts
			WHERE 1=1 AND ((lower($wpdb->posts.post_title) like %s))
			AND $wpdb->posts.post_type IN ('post', 'page', 'attachment', 'galleries')
			AND (post_status = 'publish')
			ORDER BY $wpdb->posts.post_date DESC
			LIMIT $limit;
		 ";

	 	$pageposts = $wpdb->get_results($wpdb->prepare($querystr, '%'.$wpdb->esc_like($s).'%'), OBJECT);
	 	
	 	if(!empty($pageposts))
	 	{
	
	 		foreach($pageposts as $result_item) 
	 		{
	 			$post=$result_item;
	 			
	 			$post_type = get_post_type($post->ID);
				$post_type_class = '';
				$post_type_title = '';
				
				if(!has_tag('nohome',$post->ID)){
				$post_thumb = array();
				if(has_post_thumbnail($post->ID, 'thumbnail'))
				{
				    $image_id = get_post_thumbnail_id($post->ID);
				    $post_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);
				    $image_alt = get_post_meta($image_id, '_wp_attachment_image_alt', true);
				    
				    if(isset($post_thumb[0]) && !empty($post_thumb[0]))
				    {
				        $post_type_class = '<img src="'.$post_thumb[0].'" alt="'.esc_attr($image_alt).'"/>';
				    }
				}
	 			
				echo '<div class="nota-video">';
				echo '<div class="post_thumb_sidebar"><a href="'.get_permalink($post->ID).'">'.$post_type_class.'</i></a></div>';
				echo '<div class="ajax_post ';
				
				if(!has_post_thumbnail($post->ID, 'thumbnail'))
				{
					echo 'no_post_img';
				}
				
				echo '">';
				echo '<a href="'.get_permalink($post->ID).'"><strong>'.$post->post_title.'</strong><br/>';
				echo '</div>';
				echo '</div>';
				}
			}
			
			echo '<li class="view_all"><a class="button" href="javascript:jQuery(\'#searchform\').submit()">'.esc_html__('Ver todos los resultados', 'grandmagazine' ).'</a></li>';
	
		}

	}
	else 
	{
		echo '';
	}
	die();

}

if ( ! function_exists('custom_post_type_show') ) {

// Register Custom Post Type
function custom_post_type_show() {

	$labels = array(
		'name'                  => _x( 'Shows', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Show', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Shows', 'text_domain' ),
		'name_admin_bar'        => __( 'Shows', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Shows', 'text_domain' ),
		'add_new_item'          => __( 'Add New Show', 'text_domain' ),
		'add_new'               => __( 'Add New Show', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Show', 'text_domain' ),
		'view_items'            => __( 'View Shows', 'text_domain' ),
		'search_items'          => __( 'Search Shows', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Subir imagen destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Show', 'text_domain' ),
		'description'           => __( 'Post Type Shows', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-admin-page',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'post_type_shows', $args );

}
add_action( 'init', 'custom_post_type_show', 0 );

}

if ( ! function_exists('custom_post_type_jobs') ) {

// Register Custom Post Type
function custom_post_type_jobs() {

	$labels = array(
		'name'                  => _x( 'Jobs', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Job', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Jobs', 'text_domain' ),
		'name_admin_bar'        => __( 'Jobs', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Jobs', 'text_domain' ),
		'add_new_item'          => __( 'Add New Job', 'text_domain' ),
		'add_new'               => __( 'Add New Job', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Job', 'text_domain' ),
		'view_items'            => __( 'View Jobs', 'text_domain' ),
		'search_items'          => __( 'Search Jobs', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Imagen destacada', 'text_domain' ),
		'set_featured_image'    => __( 'Subir imagen destacada', 'text_domain' ),
		'remove_featured_image' => __( 'Eliminar imagen destacada', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Job', 'text_domain' ),
		'description'           => __( 'Post Type Jobs', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title'),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-businessman',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'post_type_jobs', $args );

}
add_action( 'init', 'custom_post_type_jobs', 0 );

}

// Add metabox url type content shows
function play_agregar_metaboxes() {
	add_meta_box( "play_metaboxes","URL redirección","play_diseno_metaboxes","post_type_shows","normal","high", null );
}
add_action("add_meta_boxes", "play_agregar_metaboxes", 0);


function play_guardar_metaboxes($post_id, $post, $update) {
	if (!isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)))
	return $post_id;

	if(!current_user_can("edit_post", $post_id))
	return $post_id;

	if(defined("DOING_AUTOSAVE") && DOING_AUTOSAVE)
	return $post_id;

	$iframe_metabox = "";

	if(isset($_POST["url-metabox"])) {
		$iframe_metabox = $_POST["url-metabox"];
	}
	update_post_meta($post_id, "url-metabox", $iframe_metabox);
}
add_action("save_post", "play_guardar_metaboxes", 10, 3);



function play_diseno_metaboxes($post) {
	wp_nonce_field(basename(__FILE__), "meta-box-nonce");

	?>

	<div>
		<label for="url-metabox">URL:</label>
			<input id="url-metabox" name="url-metabox" type="url" placeholder="http://www.example.com" value="<?php echo esc_html(get_post_meta( $post->ID, 'url-metabox', true )); ?>" style="width: 100%; margin-top: 10px;" required>
        <br/>
	</div>

	<?php 
}

// reder shows home
function grandmagazine_filter_shows($items = 4, $echo = TRUE, $show_thumb = TRUE) 
{
	$return_html = '';
	$posts = get_posts('numberposts='.$items.'&order=DESC&orderby=date&post_type=post_type_shows');
	$count_post = count($posts);
	
	if(!empty($posts))
	{

		$return_html.= '<div class="posts_show">';

			foreach($posts as $key => $post)
			{
				$return_html.= '<div class="show">';
			
				if(!empty($show_thumb) && has_post_thumbnail($post->ID, 'shows'))
				{
					$image_id = get_post_thumbnail_id($post->ID);
					$image_url = wp_get_attachment_image_src($image_id, 'full', true);
					$mobileimg_id = get_post_meta($post->ID,'mobile_image_show',true);
					$desktop_image = esc_url($image_url[0]);
					$mobile_image = wp_get_attachment_url( $mobileimg_id, 'full' );
					$return_html.= '<div class="contenedor_imagen"><a href="'.get_post_meta($post->ID, 'url-metabox', true).'" target="_blank"><picture>
<source media="(max-width: 420px)" srcset="'.$mobile_image.'">
<source media="(max-width: 799px)" srcset="'.$desktop_image.'">
<source media="(min-width: 800px)" srcset="'.$desktop_image.'">
<img src="'.$desktop_image.'" alt="">
</picture><div class="overlay"><div class="text">'.grandmagazine_substr($post->post_title, 50).'</div></div></a></div>';
				}
				$return_html.= '</div>';
			}	

		$return_html.= '<br class="clear"/></div>';

	}
	
	if($echo)
	{
		echo stripslashes($return_html);
	}
	else
	{
		return $return_html;
	}
}

// Registramos el widget
function pswid_load_widget() {
	register_widget( 'pswid_widget' );
}
add_action( 'widgets_init', 'pswid_load_widget' );

// Creamos el widget 
class pswid_widget extends WP_Widget {

function __construct() {
parent::__construct(

// El ID de nuestro widget
'pswid_widget', 

// El nombre con el cual aparecerá en el backoffice de WP
__('Shows widget', 'pswid_widget_domain'), 

// Descripción del widget
array( 'description' => __( 'Trae un listado del tipo de contenido Shows', 'pswid_widget_domain' ), ) 
);
}

// Creamos la parte pública del widget

public function widget( $args, $instance ) {
$title = apply_filters( 'widget_title', $instance['title'] );

// los argumentos del antes y después del widget vienen definidos por el tema
echo $args['before_widget'];
if ( ! empty( $title ) )
echo $args['before_title'] . $title . $args['after_title'];

// Aquí es donde debemos introducir el código que queremos que se ejecute
echo grandmagazine_filter_shows();
echo $args['after_widget'];
}
// Backend  del widget
public function form( $instance ) {
if ( isset( $instance[ 'title' ] ) ) {
$title = $instance[ 'title' ];
}
else {
$title = __( 'Titulo', 'pswid_widget_domain' );
}
// Formulario del backend
?>
<p>
<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Titulo:' ); ?></label> 
<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
</p>
<?php 
}
	
// Actualizamos el widget reemplazando las viejas instancias con las nuevas
public function update( $new_instance, $old_instance ) {
$instance = array();
$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
return $instance;
}
		
} // La clase wp_widget termina aquí

function user_can_richedit_custom() {
        global $wp_rich_edit;

        if (get_user_option('rich_editing') == 'true' || !is_user_logged_in()) {
                $wp_rich_edit = true;
                return true;
        }

        $wp_rich_edit = false;
        return false;
}

add_filter('user_can_richedit', 'user_can_richedit_custom');

add_filter( 'wp_trim_excerpt', 'pg_excerpt', 10, 2 );
 
function pg_excerpt($text, $raw_excerpt) {
    if( ! $raw_excerpt ) {
        $content = apply_filters( 'the_content', get_the_content() );
        $text = substr( $content, 0, strpos( $content, '</p>' ) + 4 );
    }
    return $text;
}
function clean_content( $content ) {
    if ( is_single() && 'post' == get_post_type() ) {
    	$extracto = substr( $content, 0, strpos( $content, '</p>' ) + 4 );
    	$custom_content = str_replace($extracto, '', $content);
    	return $custom_content;
    } else {
        return $content;
    }
}
//add_filter( 'the_content', 'clean_content' );
function ad_class_dev( $classes ) { 
	$classes[] = 'ad_spain';
	return $classes;
}

function get_post_primary_category( $post = 0, $taxonomy = 'category' ){
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
        $primary_term['url']   = $term_link;
        $primary_term['slug']  = $term_slug;
        $primary_term['title'] = $term_display;
    }
    return $primary_term;
}

add_shortcode('pg-dailymotion', function($atts){
    $html = '';
    $idDLM = $atts['id'];
    if(!empty($idDLM)){
    	$html .= '<div class="video_wrapper_responsive">';
    	$html .= '<iframe frameborder="0" src="https://www.dailymotion.com/embed/video/'.$idDLM.'?autoplay=true&amp;mute=true&amp;queue-autoplay-next=false&amp;queue-enable=false&amp;sharing-enable=false" allowfullscreen="" allow="autoplay">';
    	$html .= '</iframe></div>';
    }
    return $html;
});
add_shortcode('pg_dmplayer',function($atts){
	$html = '';
	$type = $atts['type'];
	if(!empty($type)){
		$urlAPIDM = 'https://api.dailymotion.com/videos?owners=x1xe01v&limit=1&tags='.$type;
		$dataDM = file_get_contents($urlAPIDM);
    	$videoDM = json_decode($dataDM);
    	foreach ($videoDM->list as $index => $video) {
        	$videoDMp = $video->id;
        	$html .= '<iframe style="width: 100%; height: 100%; position: absolute; left: 0px; top: 0px; overflow: hidden;" src="https://www.dailymotion.com/embed/video/'.$videoDMp.'" width="100%" height="100%" frameborder="0" allowfullscreen="allowfullscreen"></iframe>';
        }
	}
	return $html;
});
add_filter('instant_articles_content','custom_pg_video',10,1);
function custom_pg_video($content){
    $id = get_the_id();
    $vidposition = get_post_meta(get_the_ID(),'video_position',true);
	$videoid = get_post_meta(get_the_ID(),'video_id',true);
    if($vidposition == 'destacado' && !empty($videoid)){
        $videjod = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$vdjsd.'?ads_params=site%3Dplayground_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $videjod.$content;
    }
    if($vidposition == 'top' && !empty($videoid)){
        $videjod = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$videoid.'?ads_params=site%3Dplayground_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $videjod.$content;
    }
    if($vidposition == 'recomendado' && !empty($videoid)){
        $videjor = '<iframe frameborder="0" width="300" height="300" src="https://www.dailymotion.com/embed/video/'.$videoid.'?ads_params=site%3Dplayground_rs&sharing-enable=false&queue-enable=false&queue-autoplay-next=false&quality=380&autoplay=true&mute=true"></iframe>';
        $content = $content.$videjor;
    }
    return $content;
}
function videos_content_amp( $content ) {
    if ( is_single() && 'post' == get_post_type() ) {
        $custom_content = "";
        $custom_content .= $content;
        $vidposition = get_post_meta(get_the_ID(),'video_position',true);
		$videoid = get_post_meta(get_the_ID(),'video_id',true);
		if ( function_exists( 'ampforwp_is_amp_endpoint' ) && ampforwp_is_amp_endpoint() ) {
            if($vidposition == 'destacado' && !empty($videoid)){
                $precontent = '<amp-dailymotion data-videoid="'.$videoid.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion></br>';
                $custom_content = $precontent.$content;
            }
            if($vidposition == 'top' && !empty($videoid)){
                $precontent = '<amp-dailymotion data-videoid="'.$videoid.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion></br>';
                $custom_content = $precontent.$content;
            }
            if($vidposition == 'recomendado' && !empty($videoid)){
                $custom_content .= '<amp-dailymotion data-videoid="'.$videoid.'" autoplay data-mute="true" layout="responsive" width="480" height="270"></amp-dailymotion>';
            }
        }
        return $custom_content;
    } else {
        return $content;
    }
}
add_filter( 'the_content', 'videos_content_amp' );

//Custom field contactform
add_action( 'wpcf7_init', 'custom_views_post_title' );
  
function custom_views_post_title() {
    wpcf7_add_shortcode( 'custom_jobs_title', 'custom_jobs_title_handler' );
}
  
function custom_jobs_title_handler( $tag ) {
    $output = '';
        $args = array(
            'numberposts'   => -1,
            'post_type'     => array( 'post_type_jobs' ),
            'post_status'   => array( 'publish' )
        );
        $the_query = new WP_Query( $args );
        if( $the_query->have_posts() ){
            $output .= '<select name="p-list-jobs" id="select-jobs"><option value="0">Selecciona una oferta</option>';
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                $idP = $the_query->post->ID;
                $title = get_the_title();
                if (get_post_meta( get_the_ID(), 'active_job', true ) == "yes") {
                		$output .= '<option value="'.$title.'">'. $title .' </option>';
                	}
            }
            $output .= "</select>";
        }
    return $output; 
 
}
//Allow disable Facebook Instant Article
add_filter("instant_articles_should_submit_post", "ad_validation_FBIA",10, 2);
function ad_validation_FBIA($should_show, $IA_object){
    $validateIA = get_post_meta($IA_object->get_the_id(), "disable_ia");
    if($validateIA){
        return false;
    }else{
        return true;
    }   
}
add_action('post_submitbox_misc_actions', 'create_disable_ia');
add_action('save_post', 'save_disable_ia');
add_action('post_submitbox_misc_actions', 'create_liveblog');
add_action('save_post', 'save_liveblog');
function create_disable_ia(){
    $post_id = get_the_ID();
  
    if (get_post_type($post_id) != 'post') {
        return;
    }
  
    $value = get_post_meta($post_id, 'disable_ia', true);
    wp_nonce_field('ad_disable_ia_nonce_'.$post_id, 'ad_disable_ia_nonce');
    ?>
    <div class="misc-pub-section misc-pub-section-last">
        <label><input type="checkbox" value="1" <?php checked($value, true, true); ?> name="disable_ia" /><?php _e('Disable this post in FB-IA', 'pmg'); ?></label>
    </div>
    <?php
}
function save_disable_ia($post_id){
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
        return;
    }
    
    if (
        !isset($_POST['ad_disable_ia_nonce']) ||
        !wp_verify_nonce($_POST['ad_disable_ia_nonce'], 'ad_disable_ia_nonce_'.$post_id)
    ) {
        return;
    }
    
    if (!current_user_can('edit_post', $post_id)) {
        return;
    }
    if (isset($_POST['disable_ia'])) {
        update_post_meta($post_id, 'disable_ia', $_POST['disable_ia']);
    } else {
        delete_post_meta($post_id, 'disable_ia');
    }
}

add_filter('instant_articles_transformed_element', function ($article) {
    $url = site_url();
    $settings_ads = Instant_Articles_Option_Ads::get_option_decoded();
    $code = new DOMDocument();
    $fragment = $code->createDocumentFragment();
    $valid_html = @$fragment->appendXML( $settings_ads['embed_code'] );
    $ad = Ad::create()
        ->withWidth(300)
        ->withHeight(250)
        ->enableDefaultForReuse();
    $article->getHeader()
        ->addAd(
            $ad->withHTML(
                $fragment
            )
        );
    return $article;
});

add_action('rest_api_init', function () {
	$ad_imgft_schema = array(
        'description'   => 'Image post featured',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'imgft_field',
        array(
            'get_callback'      => 'ad_get_imgft_field',
            'update_callback'   => null,
            'schema'            => $ad_imgft_schema
        )
    );
    $ad_videod_schema = array(
        'description'   => 'Featured video DM',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'videod',
        array(
            'get_callback'      => 'ad_get_videod_field',
            'update_callback'   => null,
            'schema'            => $ad_videod_schema
        )
    );
    $ad_videord_schema = array(
        'description'   => 'Related video DM',
        'type'          => 'string',
        'context'       =>   array( 'view' )
    );
    register_rest_field(
        'post',
        'videord',
        array(
            'get_callback'      => 'ad_get_videord_field',
            'update_callback'   => null,
            'schema'            => $ad_videord_schema
        )
    );
});
function ad_get_imgft_field( $object, $field_name, $request ) {
    return get_the_post_thumbnail_url( $object['id'], 'full' );
}
function ad_get_videod_field( $object, $field_name, $request ) {
    $dm = get_post_meta($object['id'],'video_destacado',true);
    if(!empty($dm)){
        $dmpid = $dm;
    }
    return $dmpid;
}
function ad_get_videord_field( $object, $field_name, $request ) {
    $dm = get_post_meta($object['id'],'video_recomendado',true);
    if(!empty($dm)){
        $dmpid = $dm;
    }
    return $dmpid;
}

add_action('init','add_adops_query');
function add_adops_query() { 
    global $wp; 
    $wp->add_query_var('adunit'); 
}
add_action('wp_head', 'adOPs_calls',10);
function adOPs_calls(){
	$adTagCall = '';
	$varcontrol = 1;
	if(is_single()){
		if(has_tag('noads') or has_tag('branded')){
            $varcontrol  = 0;
        }
        $tempID = get_the_ID();
        $prm_ct = get_post_primary_category($tempID, 'category');
        if(!empty($prm_ct)){
            $temp_section = $prm_ct['slug'];
            $section = $temp_section;

        }
        $author_id = get_post_field( 'post_author',$tempID );
        $author_geo = get_the_author_meta( 'geo_business', $author_id );
	}
	if(is_page()){
		$varcontrol = 0;
		$typep = 'page';
	}
	if(is_404()){
		$varcontrol = 0;
	}
	if($varcontrol == 1){
		$tags = '<script>';
		$tags .= '(function($) {
		var defaultSizesTop = [[728, 90],[970,90]];
		var defaultSizesMid = [[728, 90],[728, 250],[970, 90],[970, 250]];
		var defaultSquare = [300, 250];
	    if ($(window).width() < 720) {
	    	defaultSizesTop = [[320, 50],[320, 100]];
	        defaultSizesMid = [[320, 50],[320, 100]];
	        defaultSquare = [[300,250],[300, 600]];
	    }
	    window.googletag = window.googletag || {cmd: []};
		googletag.cmd.push(function() { '; 
		if(get_query_var('adunit') == "pg_pruebas"){
			//$tags .= 'googletag.defineSlot("/4923229/pg_pruebas", defaultSizesMegabanner, "pg_megabanner").setTargeting("position", "megabanner").addService(googletag.pubads());if ($(window).width() > 1220) {googletag.defineSlot("/4923229/pg_pruebas", [[120, 800], [120, 600]], "pg_sky_izq").setTargeting("position", "sky_izq").addService(googletag.pubads());googletag.defineSlot("/4923229/pg_pruebas", [[120, 801],[120, 800], [120, 600]], "pg_sky_der").setTargeting("position", "sky_der").addService(googletag.pubads());};';
		    $tags .= 'googletag.defineSlot("/4923229/pg_pruebas", defaultSizesMegabanner, "pg_megabanner2").setTargeting("position", "megabanner_2").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/pg_pruebas", defaultSizesCintillo, "pg_cintillo").setTargeting("position", "cintillo").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/pg_pruebas", [[300, 600],[300, 250]], "pg_robapagina").setTargeting("position", "robapaginas").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/pg_pruebas", [[300, 600],[300, 250]], "pg_robapagina_2").setTargeting("position", "robapaginas_2").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/pg_pruebas", [[300, 600],[300, 250]], "pg_robapagina_3").setTargeting("position", "robapaginas_3").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/pg_pruebas",[[300, 250], "fluid"], "pg_boton_native").setTargeting("position", "boton_native").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/pg_pruebas", [1, 1], "pg_intext").setTargeting("position", "intext").addService(googletag.pubads());';
		}else{
		    $tags .= 'googletag.defineSlot("/4923229/playgroundweb_topbanner_atf", defaultSizesTop, "pg_cintillo").setTargeting("position", "cintillo").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/playgroundweb_robapagina_1", [[300, 600],[300, 250]], "pg_robapagina").setTargeting("position", "robapaginas").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/playgroundweb_robapagina_2", defaultSquare , "pg_robapagina_2").setTargeting("position", "robapaginas_2").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/playgroundweb_robapagina_3", [[300, 600],[300, 250]], "pg_robapagina_3").setTargeting("position", "robapaginas_3").addService(googletag.pubads());
		    googletag.defineSlot("/4923229/playgroundweb_1x1",  [1, 1], "pg_1x1").setTargeting("position", "1x1").addService(googletag.pubads());';
		}
		if(is_single()){
			$tags .= 'googletag.defineSlot("/4923229/playgroundweb_native_intext", [1, 1], "pg_intext").setTargeting("position", "intext").addService(googletag.pubads());';
			$tags .= 'googletag.pubads().setTargeting("contenido_pg","'.$author_geo.'");';
		}
	    $tags .= 'googletag.pubads().enableSingleRequest();';
	    if(is_front_page()){
	    	$typep = 'home';
	    	$tags .= 'googletag.defineSlot("/4923229/playgroundweb_midbanner", defaultSizesMid, "pg_megabanner2").setTargeting("position", "megabanner_2").addService(googletag.pubads());'; 
	    	$tags .= 'googletag.pubads().setTargeting("contenido_pg","general");';   	
	    }
	    if(is_category()){
	    	$catSite = get_category( get_query_var( 'cat' ), false );
            if ( ! empty( $catSite ) ) {
                $temp_section  = $catSite->slug; 
                $section = $temp_section;
            }
            $tags .= 'googletag.defineSlot("/4923229/playgroundweb_midbanner", defaultSizesMid, "pg_megabanner2").setTargeting("position", "megabanner_2").addService(googletag.pubads());';
            $tags .= 'googletag.pubads().setTargeting("contenido_pg","general");';
            $typep = 'category';
	    }
	    $tags .= 'googletag.pubads().setTargeting("site","playgroundbr");';
        if(is_front_page()){
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag","home");';
        }else{
            $tags .= 'googletag.pubads().setTargeting("tipo_de_pag","'.$typep.'");';
            $tags .= 'googletag.pubads().setTargeting("category","'.$section.'");';
        }
        $tags .= 'googletag.pubads().collapseEmptyDivs();';
	    $tags .= 'googletag.enableServices(); }); })(jQuery);';
		$tags .= '</script>';
		echo $tags."\n";
	}
}

function remove_editors_manage_categories() {
    // get_role returns an instance of WP_Role.
    $role = get_role( 'editor' );
    $role->remove_cap( 'manage_categories' );
}
remove_editors_manage_categories();

function add_meta_robots_tags($robots) {
	if(is_single()){
		if(has_tag('exclude')){
			return 'noindex, nofollow';
		}
	}
	return $robots;
}
add_filter("wpseo_robots", 'add_meta_robots_tags');
?>
