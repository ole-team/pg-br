<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

/**
*	Get current page id
**/

$current_page_id = $post->ID;

if($post->post_type == 'galleries')
{
	//Get gallery template
	$gallery_template = get_post_meta($current_page_id, 'gallery_template', true);
	
	switch($gallery_template)
	{	
		case 'Gallery 2 Columns':
			get_template_part("gallery-2");
		break;
		
		case 'Gallery 3 Columns':
		default:
			get_template_part("gallery-3");
		break;
		
		case 'Gallery 4 Columns':
			get_template_part("gallery-4");
		break;
	}

	exit;
}
else
{
	if ((get_post_meta($post->ID, 'type_content_pg', true) == 'noticia' OR get_post_meta($post->ID, 'type_content_pg', true) == 'news')) {
		get_template_part('single-post-noticia');
		} else if ((get_post_meta($post->ID, 'type_content_pg', true) == 'entrevista' OR get_post_meta($post->ID, 'type_content_pg', true) == 'interview')) {
			get_template_part('single-post-entrevista');
		} else if (get_post_meta($post->ID, 'type_content_pg', true) == 'opinion') {
			get_template_part('single-post-opinion');
		} else if ((get_post_meta($post->ID, 'type_content_pg', true) == 'reportaje' OR get_post_meta($post->ID, 'type_content_pg', true) == 'reportage')) {
			get_template_part('single-post-reportaje');
		} else if (get_post_type() == 'post_type_shows') {
			get_template_part('single-post-shows');
		}
}
?>