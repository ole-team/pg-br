<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

get_template_part("header-internas"); 

$grandmagazine_topbar = grandmagazine_get_topbar();

/**
*	Get current page id
**/

$current_page_id = $post->ID;

/**
*	Get current page id
**/

$current_page_id = $post->ID;

//Include custom header feature
get_template_part("/templates/single_post/template-post-header-opinion");
?>

<div class="inner">

	<!-- Begin main content -->
	<div class="inner_wrapper">
		<!-- Advertising -->
		<div class="contenedor_ad_top_cintillo">
			<!-- /4923229/pg_cintillo_atf -->
			<div id='pg_cintillo' style="min-height: 90px;">
				<script>
					googletag.cmd.push(function() { googletag.display('pg_cintillo'); });
				</script>
			</div>
		</div>

		<div class="sidebar_content">
					
<?php
if (have_posts()) : while (have_posts()) : the_post();
?>
						
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<?php
	    		//Get post featured content
			    get_template_part("/templates/single_post/template-post-featured-content");
				?>
				<div class="content">			
				<?php 
					the_content();
				?>
				</div>
				<?php 
				wp_link_pages();
				
				//Get ads after content
				echo do_shortcode(grandmagazine_get_ads('pp_ads_single_after_content'));

				//Get post tags
			    get_template_part("/templates/single_post/template-post-tags");
			?>
			<div class="left">
			    <?php 
				    //Share social media
				    get_template_part("/templates/single_post/template-share-social-media");
				?>
			</div>
			<?php
			//Get author info
			    get_template_part("/templates/single_post/template-info-autor");
			?>
			<?php if(has_tag('branded','noads')){ }else{ ?>
				<div class="widget_below_content" style="padding-top: 15px;">
				<div id="taboola-below-article-thumbnails"></div>
				<script type="text/javascript">
				  window._taboola = window._taboola || [];
				  _taboola.push({
				    mode: 'thumbnails-d-network',
				    container: 'taboola-below-article-thumbnails',
				    placement: 'Below Article Thumbnails',
				    target_type: 'mix'
				  });
				</script>

			</div>
			<?php } ?>
			
			<br class="clear"/>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->

<?php endwhile; endif; ?>
						
    	</div>

    		<div class="sidebar_wrapper">
    		
    			<div class="sidebar_top"></div>
    		
    			<div class="sidebar">
    			
    				<div class="content">

    					<?php 
						if (is_active_sidebar('single-post-sidebar')) { ?>
		    	    		<ul class="sidebar_widget">
		    	    		<?php dynamic_sidebar('single-post-sidebar'); ?>
		    	    		</ul>
		    	    	<?php } ?>
    				
    				</div>
    		
    			</div>
    			<br class="clear"/>
    	
    			<div class="sidebar_bottom"></div>
    		</div>
    </div>
    <!-- End main content -->
   
</div>

<br class="clear"/>

</div>

<?php get_footer(); ?>