<?php
/**
 * The main template file for display blog page.
 *
 * @package WordPress
*/

/**
*	Get Current page object
**/
if(!is_null($post))
{
	$page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*	Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 

$is_standard_wp_post = FALSE;

if(is_tag())
{
    $is_standard_wp_post = TRUE;
} 
elseif(is_category())
{
    $is_standard_wp_post = TRUE;
}
elseif(is_archive())
{
    $is_standard_wp_post = TRUE;
} 

//Include post featured section
get_template_part("/templates/template-featured-posts");


if(is_category() OR is_tag() OR is_archive() OR is_search())
{
	get_template_part("/templates/template-header");
}
else
{
?>
<!--Begin originals box-->
<div class="shows">
<?php get_template_part("/templates/template-shows"); ?>
</div>
<!--Begin popular box-->
<div id="home_popular_box">
<div id="page_content_wrapper">
<?php
}
?>
  
    <div class="inner three_cols">

    	<!-- Begin main content -->
    	<div class="inner_wrapper">
	    	
	    		<?php
				 	grandmagazine_set_blog_layout('blog_newspaper');
				 
				 	//Include post filter bar
				 	get_template_part("/templates/template-filter");
				?>

    			<div class="sidebar_content full_width three_cols">
	    			<div id="post_filterable_wrapper" class="three_cols">
<?php
//Include post search bar
get_template_part("/templates/template-search");

$featured_posts = grandmagazine_get_featured_posts();
/*if(!empty($featured_posts) && is_array($featured_posts))
{
	
}

$wp_query = grandmagazine_get_wp_query();
$count_all_posts = $wp_query->post_count;*/
$query_arr = array(
	'post__not_in' => $featured_posts,
	'post_per_page' => 5,
	'suppress_filters' => false,
	'tag__not_in' => array(10,8),
	'cat' => array(2,3,4,5,6,7,13)

);
query_posts($query_arr);
$key = 0;
if (have_posts()) : while (have_posts()) : the_post();
	$image_thumb = '';
	$key++;
	$image_id = get_post_thumbnail_id(get_the_ID());
	
	if($key <= 1)
	{
?>

<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header featured_posts_filter mixed">
	    	
	    		<?php
		    		$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    		$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
				    		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories($post->ID);
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat_inverse">
					    <?php
					    	$primaryCat = get_post_primary_category($post->ID,'category');
					    	/*$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
						    }*/
						?>
						<a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
					</div>
					<?php
						}
					?>
			      	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true); if(!empty($custom_style)){ echo $custom_style;}else{the_title();} ?></a></h2>
			      	<div class="post_detail post_date">
			      		<span class="post_info_author_inverse">
			      			<?php
			      				$autor_custom = get_post_meta(get_the_ID(), 'autor_pg', true);
					  			if(empty($autor_custom)){
			      					$author_name = get_the_author();
			      				}else{
			      					$author_name = $autor_custom;
			      				}
			      			?>
			      			<?php echo esc_html($author_name); ?>
			      		</span>
			      		<span class="post_info_date_inverse">
					  		<span>
				  				<?php echo get_the_date( ' F d, Y' ); ?>
					  		</span>
			      		</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php
		}
		else if($key <= 2)
		{?>
<div class="post type-post">
	<!-- Advertising -->
	<div class="contenedor_ad box_ad_cp">
		<!-- Adunit 300x250 pg_boton_native -->
		<div id='pg_robapagina_2' style="min-height: 250px;">
			<script>
				googletag.cmd.push(function() { googletag.display('pg_robapagina_2'); });
			</script>
		</div>
		<span class="tt_box_ad dark">PUBLICIDAD</span>
	</div>
</div>


<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> data-column="last">

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header featured_posts_filter mixed">
	    	
	    		<?php
		    		$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    		$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
				    		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories($post->ID);
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat_inverse">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
						    }
						?>
						<a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					</div>
					<?php
						}
					?>
			      	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true); if(!empty($custom_style)){ echo $custom_style;}else{the_title();} ?></a></h2>
			      	<div class="post_detail post_date">
			      		<span class="post_info_author_inverse">
			      			<?php
			      				$autor_custom = get_post_meta(get_the_ID(), 'autor_pg', true);
					  			if(empty($autor_custom)){
			      					$author_name = get_the_author();
			      				}else{
			      					$author_name = $autor_custom;
			      				}
			      			?>
			      			<?php echo esc_html($author_name); ?>
			      		</span>
			      		<span class="post_info_date_inverse">
					  		<span>
				  				<?php echo get_the_date( ' F d, Y' ); ?>
					  		</span>
			      		</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>

<!-- Advertising 1-->
		<?php } else if($key >3 && $key <= 6){ ?>
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header featured_posts_filter mixed">
	    	
	    		<?php
		    		$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    		$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				?>
				
				     <div class="post_img static">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
				    		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					     </a>
				     </div>
					 <br class="clear"/>
				<?php
				     }
				?>
	    	
			   <div class="post_header_title">
				   	<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories($post->ID);
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat_inverse">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
						    }
						?>
						<a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					</div>
					<?php
						}
					?>
			      	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true); if(!empty($custom_style)){ echo $custom_style;}else{the_title();} ?></a></h2>
			      	<div class="post_detail post_date">
			      		<span class="post_info_author_inverse">
			      			<?php
			      				$autor_custom = get_post_meta(get_the_ID(), 'autor_pg', true);
					  			if(empty($autor_custom)){
			      					$author_name = get_the_author();
			      				}else{
			      					$author_name = $autor_custom;
			      				}
			      			?>
			      			<?php echo esc_html($author_name); ?>
			      		</span>
			      		<span class="post_info_date_inverse">
					  		<span>
				  				<?php echo get_the_date( ' F d, Y' ); ?>
					  		</span>
			      		</span>
				  	</div>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
		<?php }
	else{
?>
<?php
		}
?>
<?php endwhile; endif; ?>
				<?php /*<div class="contenedor_ad" data-column="last">
					<div id='pg_robapagina_2'>
						<script>
							googletag.cmd.push(function() { googletag.display('pg_robapagina_2'); });
						</script>
					</div>
				</div> */ ?>
	    	</div>    		
			</div>
    		<!-- Advertising New-->
    		<div class="contenedor_ad_top_cintillo">
				<!-- /4923229/pg_megabanner_atf -->
				
				<div id='pg_megabanner2' class="box_ad_cp" style="min-height: 250px;">
					<script>
						googletag.cmd.push(function() { googletag.display('pg_megabanner2'); });
					</script>
				</div>
				<span class="tt_box_ad dark">PUBLICIDAD</span>
			</div>
    	</div>
    <!-- End main content -->
	</div>
</div>
</div><!--end popular box-->
<div class="watch">
<?php
 get_template_part("/templates/template-watch"); ?>
</div>
<!--end watch box-->
<div id="page_content_wrapper" class="scroll">
	<div class="inner three_cols">

    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    	<?php
			grandmagazine_set_blog_layout('blog_newspaper');
		?>
			<div class="sidebar_content list_bottom_home">
    			<div id="post_filterable_wrapper_scroll">
<?php
//Include post search bar
get_template_part("/templates/template-search");

$featured_posts = grandmagazine_get_featured_posts();
if(!empty($featured_posts) && is_array($featured_posts))
{
	$query_arr = array(
		'post__not_in' => $featured_posts,
		'paged' => $paged,
		'suppress_filters' => false,
		'offset'=> 5,
		'tag__not_in' => array(10,8),
		'cat' => array(2,3,4,5,6,7)
	);
    query_posts($query_arr);
}

$wp_query = grandmagazine_get_wp_query();
$count_all_posts = $wp_query->post_count;

$key = 0;
if (have_posts()) : while (have_posts()) : the_post();
	$image_thumb = '';
	$key++;
	$image_id = get_post_thumbnail_id(get_the_ID());
?>

<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header search">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				        $post_content_class = 'two_third last';
				?>
				
				    <div class="post_img static one_third">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status($post->ID);
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					     </a>
				     </div>
				
				<?php
				    }
				?>
				
				<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
					<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories($post->ID);
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true); if(!empty($custom_style)){ echo $custom_style;}else{the_title();} ?></a></h5>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->
<?php endwhile; endif; ?>
		    		</div>  
			    	<?php
						//Include pagination
						get_template_part("/templates/template-pagination");
			    	?>  		
				</div>
				<div class="sidebar_wrapper">
	    			<div class="sidebar">
	    				<div class="content">
	    				<div class="contenedor_ad box_ad_cp" data-column="last">
							<!-- /4923229/robapagina -->
							<div id="pg_robapagina_3" style="min-height: 250px;">
								<script>
									googletag.cmd.push(function() {
										googletag.display("pg_robapagina_3");
									});
								</script>
							</div>
							<span class="tt_box_ad dark">PUBLICIDAD</span>
						</div>
	    				</div>
	    			</div>
	    			<br class="clear"/>
	    		</div>
    		</div>
    <!-- End main content -->
	</div>
</div>

<?php get_footer(); ?>