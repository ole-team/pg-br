<?php 
/*
Template Name: Staff

*/
if(!is_null($post))
{
	$page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*	Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 
?>

<?php
    //Include custom header feature
	get_template_part("/templates/template-header");
?>

    <div class="inner">
    	<!-- Begin main content -->
    	<div class="inner_wrapper">
    		<div class="sidebar_content full_width">
    		<?php 
            $listUsers = array(49,48,3,9,4,50,17,29);
            foreach ($listUsers as $user) {
                $author_name = get_the_author_meta('first_name', $user);
                $author_lastname = get_the_author_meta('last_name', $user);
                $author_bio = get_the_author_meta('description', $user);
                $author_link = get_author_posts_url($user);
                //if(!empty($author_bio)){ ?>
                <div class="post_info_author_bottom" style="border-top: none; border-bottom: none;"> 
                <?php
                $avatarlink = get_avatar($user);
                if(empty($avatarlink)){
                    echo '<div class="avatar_author"><img src="https://static.playground.media/wp-content/uploads/2020/11/avatar_pg-1-150x150.jpg" /></div>';
                }else{
                    echo '<div class="avatar_author">'.get_avatar($user, '100' ).'</div>';
                }
                echo '<div class="col_info_author"><a href="'.$author_link.'"><h2>'.$author_name.' '.$author_lastname.'</h2></a><p>'.$author_bio.'</p></div>';
                ?>
                    
                </div>
            <?php //}
            }
    		?>
    		
    		</div>
    	</div>
    	<!-- End main content -->
    </div> 
</div>
<br class="clear"/><br/>
<?php get_footer(); ?>