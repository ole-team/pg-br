<?php

/**
*	Begin Categories Custom Widgets
**/

class Grand_Magazine_Categoriess extends WP_Widget {
	function __construct() {
		$widget_ops = array('classname' => 'Grand_Magazine_Categoriess', 'description' => 'The categories with featured image' );
		parent::__construct('Grand_Magazine_Categoriess', 'Custom Categories With Featured Image', $widget_ops);
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);
		$title = $instance['title'];

		echo stripslashes($before_widget);
		
		if(!empty($title))
		{
			echo '<h2 class="widgettitle"><span>'.$title.'</span></h2>';
		}
		
		$categories_arr = get_categories();
		if(!empty($categories_arr))
		{
			echo '<ul>';
			$contador_categorias = 0;
			foreach($categories_arr as $category) 
			{
				if ($contador_categorias < 4) {

					$category_id = $category->term_id;
					$category_link = get_category_link($category_id);
					$category_name = $category->name;
					$category_image = z_taxonomy_image_url($category_id);
					
					if(function_exists('z_taxonomy_image_url') && !empty($category_image))
					{
						$image_id = grandmagazine_get_image_id($category_image);
						$obj_image = wp_get_attachment_image_src($image_id, 'grandmagazine_blog');
						$obj_image = grandmagazine_filter_default_featued_image($obj_image);
						
						echo '<li>';
						echo '<a href="'.esc_url($category_link).'"';
						echo ' style="background-image:url('.esc_url($obj_image[0]).');">';
						echo '<div class="background_overlay"></div>';
						echo '</a>';
						echo '</li>';
					}
					$contador_categorias++;
				}
			}
			echo '</ul>';
		}

		echo stripslashes($after_widget);
	}

	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);

		return $instance;
	}

	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '') );
		$title = strip_tags($instance['title']);
?>
			<p><label for="<?php echo esc_attr($this->get_field_id('title')); ?>">Title: <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>" name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
<?php
	}
}

register_widget('Grand_Magazine_Categoriess');

/**
*	End Categories Custom Widgets
**/
?>