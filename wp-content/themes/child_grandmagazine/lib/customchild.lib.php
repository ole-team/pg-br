<?php

function grandmagazine_cat_posts($cat_id = '', $items = 5, $echo = TRUE, $show_thumb = TRUE) 
{
	$return_html = '';
	$posts = get_posts('numberposts='.$items.'&order=DESC&orderby=date&category='.$cat_id);
	$title = get_cat_name($cat_id);
	$category_link = get_category_link($cat_id);
	$count_post = count($posts);
	$title_custom = "🔥 Watch this";
	
	if(!empty($posts))
	{

		$return_html.= '<div class="post_category_thumbnail"><h2 class="widgettitle"><span>'.$title_custom.'</span></h2>';
		$return_html.= '<div class="posts blog ';
		
		if($show_thumb)
		{
			$return_html.= 'withthumb ';
		}
		
		$return_html.= '">';

			foreach($posts as $key => $post)
			{
				$return_html.= '<div class="nota">';
			
				if(!empty($show_thumb) && has_post_thumbnail($post->ID, 'videos'))
				{
					$image_id = get_post_thumbnail_id($post->ID);
					$image_url = wp_get_attachment_image_src($image_id, 'videos', true);
					
					$return_html.= '<div class="post_thumb_sidebar"><a href="'.get_permalink($post->ID).'"><img class="frame post_thumb" src="'.esc_url($image_url[0]).'" alt="" /><div class="overlay"><div class="text"><i class="fa fa-play" aria-hidden="true"></i></div></div></a></div>';
				}
				
				$return_html.= '<div class="title"><a href="'.get_permalink($post->ID).'">'.grandmagazine_substr($post->post_title, 50).'</a></div>';
				$return_html.= '</div>';
			}	

		$return_html.= '<br class="clear"/></div>';

	}
	
	if($echo)
	{
		echo stripslashes($return_html);
	}
	else
	{
		return $return_html;
	}
}

function grandmagazine_cat_posts_home($cat_id = '', $items = 4, $echo = TRUE, $show_thumb = TRUE) 
{
	$return_html = '';
	$posts = get_posts('numberposts='.$items.'&offset=0&order=DESC&orderby=date&category='.$cat_id);
	$title = get_cat_name($cat_id);
	$category_link = get_category_link($cat_id);
	$count_post = count($posts);
	
	if(!empty($posts))
	{

		$return_html.= '<div class="post_category_thumbnail">';

			foreach($posts as $key => $post)
			{
				$return_html.= '<div class="nota-video">';
			
				if(!empty($show_thumb) && has_post_thumbnail($post->ID, 'videos'))
				{
					$image_id = get_post_thumbnail_id($post->ID);
					$image_url = wp_get_attachment_image_src($image_id, 'videos', true);
					
					$return_html.= '<div class="post_thumb_sidebar"><a href="'.get_permalink($post->ID).'"><img class="frame post_thumb" src="'.esc_url($image_url[0]).'" alt="" /><div class="overlay"><div class="text"><i class="fa fa-play" aria-hidden="true"></i></div></div></a></div>';
				}
				
				$return_html.= '<div class="title"><a href="'.get_permalink($post->ID).'">'.grandmagazine_substr($post->post_title, 50).'</a></div>';
				$return_html.= '</div>';
			}	

		$return_html.= '<br class="clear"/></div>';

	}
	
	if($echo)
	{
		echo stripslashes($return_html);
	}
	else
	{
		return $return_html;
	}
}

function grandmagazine_cat_posts_home_otros($cat_id = '', $items = 4, $echo = TRUE, $show_thumb = TRUE) 
{
	$return_html = '';
	$posts = get_posts('numberposts='.$items.'&offset=3&order=DESC&orderby=date&category='.$cat_id);
	$title = get_cat_name($cat_id);
	$category_link = get_category_link($cat_id);
	$count_post = count($posts);
	
	if(!empty($posts))
	{

		$return_html.= '<div class="post_category_thumbnail">';

			foreach($posts as $key => $post)
			{
				$return_html.= '<div class="nota-video">';
			
				if(!empty($show_thumb) && has_post_thumbnail($post->ID, 'thumbnail'))
				{
					$image_id = get_post_thumbnail_id($post->ID);
					$image_url = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					
					$return_html.= '<div class="post_thumb_sidebar"><a href="'.get_permalink($post->ID).'"><img class="frame post_thumb" src="'.esc_url($image_url[0]).'" alt="" /><div class="overlay"><div class="text"><i class="fa fa-play" aria-hidden="true"></i></div></div></a></div>';
				}
				$return_html.= '<div class="description">';
				$return_html.= '<div class="category"><a href="'.$category_link.'">'.$title.'</a></div>';
				$return_html.= '<div class="title"><a href="'.get_permalink($post->ID).'">'.grandmagazine_substr($post->post_title, 50).'</a></div>';
				$return_html.= '</div>';
				$return_html.= '</div>';
			}	

		$return_html.= '<br class="clear"/></div>';

	}
	
	if($echo)
	{
		echo stripslashes($return_html);
	}
	else
	{
		return $return_html;
	}
}

function grandmagazine_cat_posts_search($cat_id = '', $items = 5, $echo = TRUE, $show_thumb = TRUE) 
{
	$return_html = '';
	$posts = get_posts('numberposts='.$items.'&order=DESC&orderby=date&category='.$cat_id);
	$title = get_cat_name($cat_id);
	$category_link = get_category_link($cat_id);
	$count_post = count($posts);
	
	if(!empty($posts))
	{

		$return_html.= '<div class="post_category_thumbnail">';

			foreach($posts as $key => $post)
			{
				$return_html.= '<div class="nota-video">';
			
				if(!empty($show_thumb) && has_post_thumbnail($post->ID, 'thumbnail'))
				{
					$image_id = get_post_thumbnail_id($post->ID);
					$image_url = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					
					$return_html.= '<div class="post_thumb_sidebar"><a href="'.get_permalink($post->ID).'"><img class="frame post_thumb" src="'.esc_url($image_url[0]).'" alt="" /></a></div>';
				}
				$return_html.= '<div class="description">';
				$return_html.= '<div class="category"><a href="'.$category_link.'">'.$title.'</a></div>';
				$return_html.= '<div class="title"><a href="'.get_permalink($post->ID).'">'.grandmagazine_substr($post->post_title, 50).'</a></div>';
				$return_html.= '</div>';
				$return_html.= '</div>';
			}	

		$return_html.= '<br class="clear"/></div>';

	}
	
	if($echo)
	{
		echo stripslashes($return_html);
	}
	else
	{
		return $return_html;
	}
}

function grandmagazine_posts($sort = 'recent', $items = 3, $echo = TRUE, $show_thumb = TRUE) 
{
	$return_html = '';
	
	if($sort == 'recent')
	{
		$posts = get_posts('numberposts='.$items.'&order=DESC&orderby=date&post_type=post&post_status=publish');
		$title = __('Popular', 'grandmagazine');
	}
	else
	{
		$wpdb = grandmagazine_get_wpdb();
		
		if(!is_numeric($items))
		{
			$items = (int)$items;
		}
		
		$posts = $wpdb->get_results($wpdb->prepare("SELECT ID, post_title, post_content FROM {$wpdb->prefix}posts WHERE post_type = 'post' AND post_status= 'publish' ORDER BY comment_count DESC LIMIT 0, %d", $items));
		$title = __('Popular Posts', 'grandmagazine'); 
	}
	
	if(!empty($posts))
	{
		$return_html.= '<h2 class="widgettitle"><span>'.$title.'</span></h2>';
		$return_html.= '<ul class="posts blog ';
		
		if($show_thumb)
		{
			$return_html.= 'withthumb ';
		}
		
		$return_html.= '">';
		
		$count_post = count($posts);

			foreach($posts as $key => $post)
			{

				$image_thumb = get_post_meta($post->ID, 'blog_thumb_image_url', true);
				$return_html.= '<li>';
				
				if(!empty($show_thumb) && has_post_thumbnail($post->ID, 'thumbnail'))
				{
					$image_id = get_post_thumbnail_id($post->ID);
					$image_url = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					
					$return_html.= '<div class="post_circle_thumb">';
					$return_html.= '<a href="'.get_permalink($post->ID).'"><img src="'.esc_url($image_url[0]).'" alt="" /></a>';
					
					if($sort == 'popular')
					{
						$return_html.= '<div class="popular_order">'.intval($key+1).'</div>';
					}
					
					$return_html.= '</div>';
				}
				
				$return_html.= '<a href="'.get_permalink($post->ID).'">'.grandmagazine_substr($post->post_title, 50).'</a>';
				$return_html.= '</li>';

			}	

		$return_html.= '</ul>';

	}
	
	if($echo)
	{
		echo stripslashes($return_html);
	}
	else
	{
		return $return_html;
	}
}
?>