<?php
/**
 * The template for displaying the footer.
 *
 * @package WordPress
 */
?>

<?php echo do_shortcode(grandmagazine_get_ads('pp_ads_global_before_footer')); ?>

<?php
	//Check if blank template
	$grandmagazine_is_no_header = grandmagazine_get_is_no_header();
	$grandmagazine_screen_class = grandmagazine_get_screen_class();
	
	if(!is_bool($grandmagazine_is_no_header) OR !$grandmagazine_is_no_header)
	{

	$grandmagazine_homepage_style = grandmagazine_get_homepage_style();
	
	//Get Footer Sidebar
	$tg_footer_sidebar = kirki_get_option('tg_footer_sidebar');
?>
<br class="clear"/>
<div class="footer_bar <?php if(isset($grandmagazine_homepage_style) && !empty($grandmagazine_homepage_style)) { echo esc_attr($grandmagazine_homepage_style); } ?> <?php if(!empty($grandmagazine_screen_class)) { ?>split<?php } ?> <?php if(empty($tg_footer_sidebar)) { ?>noborder<?php } ?>">

	<div class="contenedor_footer">
		<div class="columns">
			<div class="column primary">
				<div class="logo">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/img/playgorund_blanco.png" alt="">
				</div>
				<div class="links">
					<a href="<?php bloginfo( 'url' ); ?>/aviso-legal/">Terms</a>
					<a href="<?php bloginfo( 'url' ); ?>/politica-de-privacidad">Privacy policy</a>
				</div>
				<div class="copy">
					<span>
						<?php esc_html_e( 'PLAYGROUND AMERICAS LLC ©2022', 'grandmagazine' ); ?>
					</span>
					<span>
						<?php esc_html_e( 'All rights reserved', 'grandmagazine' ); ?>
					</span>
				</div>
			</div>
			<div class="column second">
				<div class="title">
					<?php esc_html_e( 'Siga a PlayGround', 'grandmagazine' ); ?>
				</div>
				<div class="share-social">
					<a class="facebook" title="Síguenos en Facebook" target="_blank" href="https://www.facebook.com/PlayGroundBR"><i class="fa fa-facebook"></i></a>
					<a class="twitter" title="Síguenos en Twitter" target="_blank" href="https://twitter.com/playgroundbr"><i class="fa fa-twitter"></i></a>
					<a class="whatsapp" title="Síguenos en instagram" target="_blank" href="https://www.instagram.com/playground.br/"><i class="fa fa-instagram"></i></a>
					<a class="mail" title="Síguenos en Youtube" href="https://www.youtube.com/c/PlayGroundBrasil"><i class="fa fa-youtube"></i></a>
				</div>
			</div>
			<div class="column third">
				<div class="title">
					<?php esc_html_e( 'Newsletter', 'grandmagazine' ); ?>
				</div>
				<div class="formulario">
					<?php echo revue_subscribe_form(); ?>
				</div>
				<div class="links_mobile">
					<a href="<?php bloginfo( 'url' ); ?>/aviso-legal/">Termos e condições</a>
					<a href="<?php bloginfo( 'url' ); ?>/politica-de-privacidad">Política de privacidade</a>
				</div>
				<div class="copy">
					<span>
						<?php esc_html_e( 'PlayGround comunications ©2022 ole comunications.', 'grandmagazine' ); ?>
					</span>
					<span>
						<?php esc_html_e( 'All rights reserved', 'grandmagazine' ); ?>
					</span>
				</div>
			</div>
		</div>
	</div>

</div>

<?php
    } //End if not blank template
?>

<div id="side_menu_wrapper" class="overlay_background">
	<!--<a id="close_mobile_menu" href="javascript:;"><i class="fa fa-close"></i></a>-->
	<?php
		if(is_single())
		{
	?>
	<div id="fullscreen_share_wrapper">
		<div class="fullscreen_share_content">
	<?php
			get_template_part("/templates/template-share");
	?>
		</div>
	</div>
	<?php
		}
	?>
</div>

<?php
    //Check if theme demo then enable layout switcher
    if(GRANDMAGAZINE_THEMEDEMO)
    {	
	    if(isset($_GET['styling']) && !empty($_GET['styling']) && file_exists(get_template_directory()."/cache/".$_GET['styling'].".css"))
	    {
		    wp_enqueue_style("grandmagazine-demo-styling", get_template_directory_uri()."/cache/".$_GET['styling'].".css", false, "", "all");
	    }
	    
	    $customizer_styling_arr = array( 
			array('id'	=>	'red', 'title' => 'Red', 'code' => '#FF3B30'),
			array('id'	=>	'orange', 'title' => 'Orange', 'code' => '#FF9500'),
			array('id'	=>	'yellow', 'title' => 'Yellow', 'code' => '#FFCC00'),
			array('id'	=>	'green', 'title' => 'Green', 'code' => '#4CD964'),
			array('id'	=>	'teal_blue', 'title' => 'Teal Blue', 'code' => '#5AC8FA'),
			array('id'	=>	'blue', 'title' => 'Blue', 'code' => '#007AFF'),
			array('id'	=>	'purple', 'title' => 'Purple', 'code' => '#5856D6'),
			array('id'	=>	'pink', 'title' => 'Pink', 'code' => '#FF2D55'),
		);
?>
    <div id="option_wrapper">
    <div class="inner">
    	<div style="text-align:center">
	    	<h5>Predefined Styling</h5>
	    	<p>
	    	Here are predefined color styling that can be imported in one click and you can also customised yours.</p>
	    	<ul class="demo_list">
		    	<?php
			    	foreach($customizer_styling_arr as $customizer_styling)
					{
				?>
	    		<li>
	        		<div class="item_content_wrapper">
						<div class="item_content">
							<a href="<?php echo esc_url(home_url('/?styling='.$customizer_styling['id'])); ?>">
					    		<div class="item_thumb" style="background:<?php echo esc_attr($customizer_styling['code']); ?>"></div>
							</a>
						</div>
					</div>		   
	    		</li>
	    		<?php
		    		}
		    	?>
	    	</ul>
    	</div>
    </div>
    </div>
    <div id="option_btn">
    	<a href="javascript:;" class="demotip" title="Choose Theme Styling"><i class="fa fa-cog"></i></a>
    	<a href="http://themegoods.theme-demo.net/grandmagazinenewsblogwordpress" class="demotip" title="Try Before You Buy" target="_blank"><i class="fa fa-edit"></i></a>
    	<a href="http://themes.themegoods.com/grandmagazine/doc/" class="demotip" title="Theme Documentation" target="_blank"><i class="fa fa-book"></i></a>
    	<a href="https://themeforest.net/item/grand-magazine-news-blog-wordpress/19054317?ref=ThemeGoods&license=regular&open_purchase_for_item_id=18556524&purchasable=source&ref=ThemeGoods" title="Purchase Theme" class="demotip" target="_blank"><i class="fa fa-shopping-basket"></i></a>
    </div>
<?php
    	wp_enqueue_script("grandmagazine-jquery-cookie", get_template_directory_uri()."/js/jquery.cookie.js", false, GRANDMAGAZINE_THEMEVERSION, true);
    	wp_enqueue_script("grandmagazine-script-demo", admin_url('admin-ajax.php')."?action=grandmagazine_script_demo", false, GRANDMAGAZINE_THEMEVERSION, true);
    }
?>
<?php if(has_tag('branded','noads')){ }else{ ?>
<script type="text/javascript">
window._taboola = window._taboola || [];
_taboola.push({flush: true});
</script>
<?php } ?>
<div id="pg_1x1">
	<script>
		googletag.cmd.push(function() { googletag.display('pg_1x1'); });
	</script>
</div>
<?php if(is_single()){ ?>
<div id="pg_intext">
	<script>
		googletag.cmd.push(function() { googletag.display('pg_intext'); });
	</script>
</div>
<?php } ?>
<?php
	/* Always have wp_footer() just before the closing </body>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to reference JavaScript files.
	 */

	wp_footer();
?>
</body>
</html>
