<?php
if(is_category()){
	$ckl = get_queried_object();
	$listCats = $ckl->term_id;
}else{
	$listCats =  array(2,3,4,5,6,7);
}
if($paged <= 1)
{
	//Check if display slider
	$featured_posts = array();
	$tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
	$count = 1;
	
	if(empty($tg_blog_featured_cat) && !is_search() && !is_category() && !is_tag() && !is_archive())
	{
		//Get post featured category
		$sticky = get_option( 'sticky_posts' );
		rsort( $sticky );
		$sticky = array_slice( $sticky, 0, 5 );
		$args = array( 
			'orderby' => 'date',
			'order' => 'DESC',
			'suppress_filters' => 0,
			'post__in' => $sticky,
			'tag__not_in' => array(10,8),
			'ignore_sticky_posts' => 1,
			'cat' => $listCats
		);
		// the query
		$theme_query = new WP_Query( $args );
		
		$count_all = $theme_query->post_count;
		if($count_all <= 0)
		{
			$args = grandmagazine_get_default_recent_posts(6);
			$theme_query = new WP_Query( $args );
			$count_all = $theme_query->post_count;
		}
		
		if ($theme_query->have_posts()) : while ($theme_query->have_posts()) : $theme_query->the_post();
		
			array_push($featured_posts, get_the_ID());
		
			//if first post
			if($count == 1)
			{
				$post_featured_image = '';			
				if(has_post_thumbnail(get_the_ID(), 'original'))
				{
				    $image_id = get_post_thumbnail_id(get_the_ID());
				    $image_thumb = wp_get_attachment_image_src($image_id, 'original', true);
				    
				    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
				    {
					    $post_featured_image = $image_thumb[0];
				    }
				}
				
				//Get post featured content
				$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);
				
				if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['content']) && $_GET['content'] == 'image')
				{
					$post_ft_type = 'Image';
				}
				
				$video_url = '';
				
				if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video')
				{
					//Add jarallax video script
					wp_enqueue_script("jarallax-video", get_template_directory_uri()."/js/jarallax-video.js", false, GRANDMAGAZINE_THEMEVERSION, true);
					
					if($post_ft_type == 'Youtube Video')
					{
						$post_ft_youtube = get_post_meta(get_the_ID(), 'post_ft_youtube', true);
						$video_url = 'https://www.youtube.com/watch?v='.$post_ft_youtube;
					}
					else
					{
						$post_ft_vimeo = get_post_meta(get_the_ID(), 'post_ft_vimeo', true);
						$video_url = 'https://vimeo.com/'.$post_ft_vimeo;
					}
				}
	?>
	<div id="featured_first_post_wrapper" <?php if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video') { ?>data-jarallax-video="<?php echo esc_url($video_url); ?>"<?php } ?>>
		<?php if(!empty($post_featured_image)) { ?>
		<style type="text/css">
		#featured_first_post_wrapper{background-image:url(<?php echo esc_url($post_featured_image); ?>);}
		<?php
		$image_id = get_post_thumbnail_id(get_the_ID());
		$featured_mb = wp_get_attachment_image_src($image_id, 'mobile-ft', true);
		if(!empty($featured_mb)){
		?>
		@media all and (max-width:768px),
		only screen and (-webkit-min-device-pixel-ratio:2) and (max-width:1024px),
		only screen and (min--moz-device-pixel-ratio:2) and (max-width:1024px),
		only screen and (-o-min-device-pixel-ratio:2/1) and (max-width:1024px),
		only screen and (min-device-pixel-ratio:2) and (max-width:1024px),
		only screen and (min-resolution:192dpi) and (max-width:1024px),
  		only screen and (min-resolution:2dppx) and (max-width:1024px) {
  			#featured_first_post_wrapper{background-image:url(<?php echo $featured_mb[0]; ?>);}
  		}
  		<?php } ?>
		</style>
		<?php } ?>
		<div class="background_overlay"></div>
		<a class="featured_first_post_link" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"></a>
		
		<div id="post-<?php the_ID(); ?>">
			<div class="post_header">
				<div class="standard_wrapper">
				    <div class="post_header_title">
				    	<?php

							//Get Post's Categories
						    $post_categories = wp_get_post_categories($post->ID);
						    
						    //Get featured post category
						    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
						    $tg_blog_featured_cat_display = kirki_get_option('tg_blog_featured_cat_display');
						    
						    if(!empty($post_categories))
						    {
						?>
						<div class="post_info_cat">
						    <?php
						    $primaryCat = get_post_primary_category($post->ID,'category');
						    /*	$i = 0;
						    	$len = count($post_categories);
						        foreach($post_categories as $c)
						        {
						        	$cat = get_category( $c );
						        	
						        	$display_featured_cat_title = false;
						        	if(!empty($tg_blog_featured_cat_display) OR (empty($tg_blog_featured_cat_display) && $cat->term_id != $tg_blog_featured_cat))
						        	{
							        	$display_featured_cat_title = true;
						        	}
							    }*/
							?>
							<a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
						</div>
						<?php
							}
						?>
						<?php
							//Get post view counter
							$post_view = grandmagazine_get_post_view($post->ID);
							
							if(!empty($post_view))
							{
						?>
						<?php
							}
							$custom_style = get_post_meta($post->ID,'title_style',true);
							if(!empty($custom_style)){
								echo '<h1>'.$custom_style.'</h1>';
								
							}else{ ?>
								<h1><?php the_title(); ?></h1>
							<?php } ?>
					  	
					  	<div class="post_detail post_date">
					  		<span class="post_info_author">
					  			<?php
					  				$autor_custom = get_post_meta(get_the_ID(), 'autor_pg', true);
					  				if(empty($autor_custom)){
					  					$author_id = get_the_author_meta('ID', $post->post_author);
							  			$author_name = get_the_author_meta('display_name', $author_id);
							  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
					  				}else{
					  					$author_name = $autor_custom;
					  				}	
							  	?>
							  	<?php echo esc_html($author_name); ?>
					  		</span>
					  		<span class="post_info_date">
					  			<span>
				  					<?php echo get_the_date( ' F d, Y' ); ?>
					  			</span>
					  		</span>
					  	</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php	
			}
			//End if first post
			else
			{
				if($count == 2)
				{
					$image_thumb = '';
								
					if(has_post_thumbnail(get_the_ID(), 'medium'))
					{
					    $image_id = get_post_thumbnail_id(get_the_ID());
					    $image_thumb = wp_get_attachment_image_src($image_id, 'medium', true);
					}
	?>
		<div id="featured_posts_wrapper">
			<div class="standard_wrapper">
				<!-- Advertising -->
				<div class="contenedor_ad_top_cintillo">
					<!-- /4923229/pg_cintillo_atf -->
					<span class="tt_box_ad dark">PUBLICIDAD</span>
					<div id='pg_cintillo' class="box_ad" style="min-height: 90px;">
					  <script>
					    googletag.cmd.push(function() { googletag.display('pg_cintillo'); });
					  </script>
					</div>
				</div>

				<div class="one_half featured_list_posted"> 
					<div class="one_half">
						<div class="post_featured">
					<div id="post-<?php the_ID(); ?>">
						<div class="post_wrapper">
		    
						    <div class="post_content_wrapper">
						    
						    	<div class="post_header featured_posts_last">
							    	<?php
								    	$image_thumb = '';
								
										if(has_post_thumbnail(get_the_ID(), 'large'))
										{
										    $image_id = get_post_thumbnail_id(get_the_ID());
										    $image_thumb = wp_get_attachment_image_src($image_id, 'large', true);
										}
					
									    if(!empty($image_thumb))
									    {
									       	$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
									       	$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
									?>
									
									   	<div class="post_img static small">
									   	    <a href="<?php the_permalink(); ?>">
									   	    	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
									   	    	
									   	    	<?php
				    		
										      		//Get post featured content
											  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
											  		
											  		switch($post_ft_type)
												    {
												    	case 'Vimeo Video':
												    	case 'Youtube Video':
												?>
															<div class="post_img_type_wrapper">
													    		<i class="fa fa-video-camera"></i>
													    	</div>
												<?php
												    	break;
												    	
												    	case 'Gallery':
												?>
												    		<div class="post_img_type_wrapper">
													    		<i class="fa fa-camera"></i>
													    	</div>
												<?php
												    	break;
												    	
												    } //End switch
										      	?>
										      	
										      	<?php
													//Get post view counter
													$post_view = grandmagazine_get_post_view($post->ID);
												
												?>
									   	    </a>
									   	</div>
								   <?php
								   		}
								   	?>
								   	<br class="clear"/>
								   	
								   	<div class="post_header_title">
								   		<?php
											//Get Post's Categories
										    $post_categories = wp_get_post_categories($post->ID);
										    
										    //Get featured post category
										    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
										    $tg_blog_featured_cat_display = kirki_get_option('tg_blog_featured_cat_display');
										    
										    if(!empty($post_categories))
										    {
										?>
										<div class="post_info_cat">
										    <?php
										    $primaryCat = get_post_primary_category($post->ID,'category');
										    	/*$i = 0;
										    	$len = count($post_categories);
										        foreach($post_categories as $c)
										        {
										        	$cat = get_category( $c );
										        	
										        	$display_featured_cat_title = false;
										        	if(!empty($tg_blog_featured_cat_display) OR (empty($tg_blog_featured_cat_display) && $cat->term_id != $tg_blog_featured_cat))
										        	{
											        	$display_featured_cat_title = true;
										        	}
											    }*/
											?>
											<a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
										</div>
										<?php
											}
										?>
								      	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true);
							if(!empty($custom_style)){
								echo $custom_style;
								
							}else {the_title();} ?></a></h2>
								      	<div class="post_detail post_date">
								      		<span class="post_info_date">
								      			<span class="post_info_author">
										  			<?php
										  				$autor_custom = get_post_meta(get_the_ID(), 'autor_pg', true);
										  				if(empty($autor_custom)){
										  					$author_id = get_the_author_meta('ID', $post->post_author);
												  			$author_name = get_the_author_meta('display_name', $author_id);
												  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
										  				}else{
										  					$author_name = $autor_custom;
										  				}	
												  	?>
												  	<?php echo esc_html($author_name); ?>
										  		</span>
								      			<span class="post_info_date">
										  			<span>
									  					<?php echo get_the_date( ' F d, Y' ); ?>
										  			</span>
										  		</span>
								      		</span>
									  	</div>
								   </div>
								</div>
								
						    </div>
						    </div>
						</div>
					</div>
					</div>
	<?php	
				} //End IF 2
				else if($count >= 2)
				{
	?>			
	<?php
				$last_class = '';
				
				if($count == 3 OR $count == 5)
				{
					$last_class = 'last';
				}
	?>			
				<div class="one_half <?php echo esc_attr($last_class); ?>">
					<div class="post_featured">
					<div id="post-<?php the_ID(); ?>">
						<div class="post_wrapper">
		    
						    <div class="post_content_wrapper">
						    
						    	<div class="post_header featured_posts_last">
							    	<?php
								    	$image_thumb = '';
								
										if(has_post_thumbnail(get_the_ID(), 'large'))
										{
										    $image_id = get_post_thumbnail_id(get_the_ID());
										    $image_thumb = wp_get_attachment_image_src($image_id, 'large', true);
										}
					
									    if(!empty($image_thumb))
									    {
									       	$small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
									       	$small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
									?>
									
									   	<div class="post_img static small">
									   	    <a href="<?php the_permalink(); ?>">
									   	    	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
									   	    	
									   	    	<?php
				    		
										      		//Get post featured content
											  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
											  		
											  		switch($post_ft_type)
												    {
												    	case 'Vimeo Video':
												    	case 'Youtube Video':
												?>
															<div class="post_img_type_wrapper">
													    		<i class="fa fa-video-camera"></i>
													    	</div>
												<?php
												    	break;
												    	
												    	case 'Gallery':
												?>
												    		<div class="post_img_type_wrapper">
													    		<i class="fa fa-camera"></i>
													    	</div>
												<?php
												    	break;
												    	
												    } //End switch
										      	?>
										      	
										      	<?php
													//Get post view counter
													$post_view = grandmagazine_get_post_view($post->ID);
												
												?>
									   	    </a>
									   	</div>
								   <?php
								   		}
								   	?>
								   	<br class="clear"/>
								   	
								   	<div class="post_header_title">
								   		<?php
											//Get Post's Categories
										    $post_categories = wp_get_post_categories($post->ID);
										    
										    //Get featured post category
										    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
										    $tg_blog_featured_cat_display = kirki_get_option('tg_blog_featured_cat_display');
										    
										    if(!empty($post_categories))
										    {
										?>
										<div class="post_info_cat">
										    <?php
										    	$primaryCat = get_post_primary_category($post->ID,'category');
										    	/*$i = 0;
										    	$len = count($post_categories);
										        foreach($post_categories as $c)
										        {
										        	$cat = get_category( $c );
										        	
										        	$display_featured_cat_title = false;
										        	if(!empty($tg_blog_featured_cat_display) OR (empty($tg_blog_featured_cat_display) && $cat->term_id != $tg_blog_featured_cat))
										        	{
											        	$display_featured_cat_title = true;
										        	}
											    }*/
											?>
											<a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
										</div>
										<?php
											}
										?>
								      	<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php $custom_style = get_post_meta($post->ID,'title_style',true);
							if(!empty($custom_style)){
								echo $custom_style;
								
							}else{the_title();} ?></a></h2>
								      	<div class="post_detail post_date">
								      		<span class="post_info_date">
								      			<span class="post_info_author">
										  			<?php
										  				$autor_custom = get_post_meta(get_the_ID(), 'autor_pg', true);
										  				if(empty($autor_custom)){
										  					$author_id = get_the_author_meta('ID', $post->post_author);
												  			$author_name = get_the_author_meta('display_name', $author_id);
												  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
										  				}else{
										  					$author_name = $autor_custom;
										  				}	
												  	?>
												  	<?php echo esc_html($author_name); ?>
										  		</span>
								      			<span class="post_info_date">
										  			<span>
									  					<?php echo get_the_date( ' F d, Y' ); ?>
										  			</span>
										  		</span>
								      		</span>
									  	</div>
								   </div>
								</div>
								
						    </div>
						    
						</div>
					</div>
					</div>
				</div>
	<?php	
				}
				
				if(($count > 6 && $count == $count_all) OR $count == $count_all)
				{
	?>
				</div>
					<div class="one_half last div_featured_advertising">
					<!-- adunit robapagina -->
					<div class="contenedor_robapagina box_ad_cp">
						<div id='pg_robapagina' style="min-height: 250px;">
						  <script>
						    googletag.cmd.push(function() { googletag.display('pg_robapagina'); });
						  </script>
						</div>
						<span class="tt_box_ad dark">PUBLICIDAD</span>
					</div>
					<div class="home-block-social">
						<div class="title">Siga a PlayGround</div>
						<div class="share-social">
							<a class="facebook" title="Facebook" target="_blank" href="https://www.facebook.com/PlayGroundBR"><i class="fa fa-facebook"></i></a>
							<a class="twitter" title="Twitter" target="_blank" href="https://twitter.com/playgroundbr"><i class="fa fa-twitter"></i></a>
							<a class="whatsapp" title="Instagram" target="_blank" href="https://www.instagram.com/playground.br/"><i class="fa fa-instagram"></i></a>
							<a class="mail" title="Youtube" href="https://www.youtube.com/c/PlayGroundBrasil"><i class="fa fa-youtube"></i></a>
						</div>
					</div>
				</div>
				
	<?php
				}
				
				if($count == $count_all)
				{
	?>		</div>
		</div>
	<?php
				}
			}
			
			$count++;
			
			endwhile; endif;
			
		wp_reset_postdata();
		
		grandmagazine_set_featured_posts($featured_posts);
?>
		<br class="clear"/>
<?php		
	} //End if display slider
}
?>

<?php echo do_shortcode(grandmagazine_get_ads('pp_ads_global_after_featured')); ?>