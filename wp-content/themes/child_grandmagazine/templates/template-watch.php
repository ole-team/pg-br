<?php 
	$category_videos = get_cat_ID('Videos');
 ?>
<div id="category" class="contenedor_videos">
	<div class="contenedor">
		<h2><?php echo esc_html_e( 'Watch This!', 'grandmagazine' ); ?></h2>
		<img src="https://static.playgroundweb.com/wp-content/uploads/2021/04/pg_emoji_watchThis.png" class="emoji_watchThis" />
			<?php 
			$the_query = new WP_Query(array('cat' => $category_videos,'tag' => 'watch-this','posts_per_page' => '1') );
			if ( $the_query->have_posts() ) :
			    while ( $the_query->have_posts() ) : $the_query->the_post();
			    	echo '<!--'.$post->ID.'--><div class="wrap_box_video"><a href="'.get_permalink().'"><div class="icon_play_box"><img src="'.get_stylesheet_directory_uri().'/img/boton-de-play.svg"/></div>';
			       	if ( has_post_thumbnail() ) { 
						$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
						echo '<img src="'.$image[0].'" />';
					} 
			        echo '<div class="tt_box_video"><h5>' . get_the_title() . '</h5></div>';
			        echo '</a></div>';
			    endwhile;
			else : ?>
			<div class="div_videos">
				<div class="wrap_video">
				<?php echo do_shortcode('[pg_dmplayer type="watchthis"]'); ?>
				</div>
			</div>
			<?php  endif; 
			wp_reset_postdata();
			?>
	</div>
</div>