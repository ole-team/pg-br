<?php
/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/

if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}

//Get page header display setting
$page_title = get_the_title();

$pp_page_bg = '';

//Get page featured image
if(has_post_thumbnail($current_page_id, 'original'))
{
    $image_id = get_post_thumbnail_id($current_page_id); 
    $image_thumb = wp_get_attachment_image_src($image_id, 'original', true);
    
    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
    {
    	$pp_page_bg = $image_thumb[0];
    }
}

//Get post featured content
$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);
$video_url = '';

if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video')
{
	//Add jarallax video script
	wp_enqueue_script("jarallax-video", get_template_directory_uri()."/js/jarallax-video.js", false, GRANDMAGAZINE_THEMEVERSION, true);
	
	if($post_ft_type == 'Youtube Video')
	{
		$post_ft_youtube = get_post_meta(get_the_ID(), 'post_ft_youtube', true);
		$video_url = 'https://www.youtube.com/watch?v='.$post_ft_youtube;
	}
	else
	{
		$post_ft_vimeo = get_post_meta(get_the_ID(), 'post_ft_vimeo', true);
		$video_url = 'https://vimeo.com/'.$post_ft_vimeo;
	}
}

$grandmagazine_topbar = grandmagazine_get_topbar();
$grandmagazine_screen_class = grandmagazine_get_screen_class();
?>
<div id="page_caption" class="<?php if(!empty($grandmagazine_topbar)) { ?>withtopbar<?php } ?> <?php if(!empty($pp_page_bg)) { ?>noborder<?php } ?>">
	
	<div class="page_title_wrapper">
		
	    <div class="page_title_inner">
	    	<div class="type-content">
	    		<span>	
	    		<?php 
	    		$type_content = get_post_meta(get_the_ID(), 'type_content_pg', true);
	    		echo $type_content;
	    		?>
	    		</span>
	    	</div>
	    	<div class="title"><h1 <?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>class ="withtopbar"<?php } ?>><?php echo esc_html($page_title); ?></h1></div>
		    <?php
				//Get Post's Categories
			    $post_categories = wp_get_post_categories($post->ID);
			    
			    //Get featured post category
			    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
			    
			    if(!empty($post_categories))
			    {
			?>
			<div class="post_info_cat">
			    <?php
			    $primaryCat = get_post_primary_category($post->ID,'category');
			    	/*$i = 0;
			    	$len = count($post_categories);
			        foreach($post_categories as $c)
			        {
			        	$cat = get_category( $c );
				    }*/
				?>
				<a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
			</div>
			<?php
				}
			?>
			<?php
				//Get post view counter
				$post_view = grandmagazine_get_post_view($post->ID);
			?>
	    	
	    	<div class="post_detail">
	    	  <span class="post_info_author">
			  	<?php
			  	$autor_old = get_post_meta(get_the_ID(), 'autor_pg', true);
		  		if(empty($autor_old)){
		  			$author_id = get_the_author_meta('ID', $post->post_author);
		  			$author_name = get_the_author_meta('display_name', $author_id);
		  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
		  		}else{
		  			$author_name = $autor_old;
		  		}
			  	?>
			  	<?php echo esc_html($author_name); ?>
			  </span>
			</div>
			<div class="date">
			  <span class="post_info_date">
			  	<span>
			  		<?php echo get_the_date( 'd F y' ); ?>
			  	</span>
			  </span>
			</div>
			<div class="share-social">
				<a class="facebook" title="<?php esc_html_e( 'Share On Facebook', 'grandmagazine' ); ?>" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>"><i class="fa fa-facebook"></i></a>
				<a class="twitter" title="<?php esc_html_e( 'Share On Twitter', 'grandmagazine' ); ?>" target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?php echo get_permalink(); ?>&url=<?php echo get_permalink(); ?>"><i class="fa fa-twitter"></i></a>
				<a class="whatsapp" title="<?php esc_html_e( 'Share On whatsapp', 'grandmagazine' ); ?>" target="_blank" href="https://api.whatsapp.com/send?<?php echo get_permalink(); ?>"><i class="fa fa-whatsapp"></i></a>
				<a class="mail" title="<?php esc_html_e('Share by Email', 'grandmagazine' ); ?>" href="mailto:someone@example.com?Subject=<?php echo rawurlencode($post->post_title); ?>&amp;Body=<?php echo rawurlencode(get_permalink($post->ID)); ?>"><i class="fa fa-envelope-o"></i></a>
			</div>
	    </div>
	    
	</div>
	<br class="clear"/>
</div>

<?php
	if(!empty($pp_page_bg)) 
	{
?>
<div class="post_featured_content_bg <?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?>" <?php if(!empty($pp_page_bg)) { ?>style="background-image:url(<?php echo esc_url($pp_page_bg); ?>);"<?php } ?> <?php if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video') { ?>data-jarallax-video="<?php echo esc_url($video_url); ?>"<?php } ?>>
	<img src="<?php echo esc_url($pp_page_bg); ?>" alt="<?php echo esc_html($page_title); ?>" />
</div>
<?php
	}
?>

<!-- Begin content -->
<?php
	$grandmagazine_page_content_class = grandmagazine_get_page_content_class();
?>
<div id="page_content_wrapper" class="<?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?><?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>withtopbar <?php } ?><?php if(!empty($grandmagazine_page_content_class)) { echo esc_attr($grandmagazine_page_content_class); } ?>">