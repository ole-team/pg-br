<div class="share-social">
	<a class="facebook" title="<?php esc_html_e( 'Share On Facebook', 'grandmagazine' ); ?>" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>"><i class="fa fa-facebook"></i></a>
	<a class="twitter" title="<?php esc_html_e( 'Share On Twitter', 'grandmagazine' ); ?>" target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?php echo get_permalink(); ?>&url=<?php echo get_permalink(); ?>"><i class="fa fa-twitter"></i></a>
	<a class="whatsapp" title="<?php esc_html_e( 'Share On whatsapp', 'grandmagazine' ); ?>" target="_blank" href="https://api.whatsapp.com/send?<?php echo get_permalink(); ?>"><i class="fa fa-whatsapp"></i></a>
	<a class="mail" title="<?php esc_html_e('Share by Email', 'grandmagazine' ); ?>" href="mailto:someone@example.com?Subject=<?php echo rawurlencode($post->post_title); ?>&amp;Body=<?php echo rawurlencode(get_permalink($post->ID)); ?>"><i class="fa fa-envelope-o"></i></a>
</div>