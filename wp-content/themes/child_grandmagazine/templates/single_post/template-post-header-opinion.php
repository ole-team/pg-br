<?php
/**
*	Get Current page object
**/
$page = get_page($post->ID);

/**
*	Get current page id
**/

if(!isset($current_page_id) && isset($page->ID))
{
    $current_page_id = $page->ID;
}

//Get page header display setting
$page_title = get_the_title();

$pp_page_bg = '';

//Get page featured image
if(has_post_thumbnail($current_page_id, 'original'))
{
    $image_id = get_post_thumbnail_id($current_page_id); 
    $image_thumb = wp_get_attachment_image_src($image_id, 'original', true);
    
    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
    {
    	$pp_page_bg = $image_thumb[0];
    }
}

//Get post featured content
$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);
$video_url = '';

if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video')
{
	//Add jarallax video script
	wp_enqueue_script("jarallax-video", get_template_directory_uri()."/js/jarallax-video.js", false, GRANDMAGAZINE_THEMEVERSION, true);
	
	if($post_ft_type == 'Youtube Video')
	{
		$post_ft_youtube = get_post_meta(get_the_ID(), 'post_ft_youtube', true);
		$video_url = 'https://www.youtube.com/watch?v='.$post_ft_youtube;
	}
	else
	{
		$post_ft_vimeo = get_post_meta(get_the_ID(), 'post_ft_vimeo', true);
		$video_url = 'https://vimeo.com/'.$post_ft_vimeo;
	}
}

$grandmagazine_topbar = grandmagazine_get_topbar();
$grandmagazine_screen_class = grandmagazine_get_screen_class();
?>
<?php
	if(!empty($pp_page_bg)) 
	{
?>

<div class="imagen_destacada">
	<div class="type_content"><span><?php echo esc_html('Opinión'); ?></span></div>
	<div class="post_featured_content_bg notitle <?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?>" <?php if(!empty($pp_page_bg)) { ?>style="background-image:url(<?php echo esc_url($pp_page_bg); ?>);"<?php } ?> <?php if($post_ft_type == 'Youtube Video' OR $post_ft_type == 'Vimeo Video') { ?>data-jarallax-video="<?php echo esc_url($video_url); ?>"<?php } ?>>
		<div class="background_overlay"></div>
		<img src="<?php echo esc_url($pp_page_bg); ?>" alt="<?php echo esc_html($page_title); ?>" />
	</div>
</div>
<?php
	}
?>

<div id="page_caption" class="static_title <?php if(!empty($grandmagazine_topbar)) { ?>withtopbar<?php } ?>">
	
	<div class="page_title_wrapper">
		
	    <div class="page_title_inner">
		    <?php
				//Get Post's Categories
			    $post_categories = wp_get_post_categories($post->ID);
			    
			    //Get featured post category
			    $tg_blog_featured_cat = kirki_get_option('tg_blog_featured_cat');
			    
			    if(!empty($post_categories))
			    {
			?>
			<div class="post_info_cat desktop">
				<!--<a href="#"><?php //echo esc_html('Opinión'); ?> </a>-->
			    <?php
			    $primaryCat = get_post_primary_category($post->ID,'category');
			    	/*$i = 0;
			    	$len = count($post_categories);
			        foreach($post_categories as $c)
			        {
			        	$cat = get_category( $c );
				    }*/
				?>
				<a href="<?php echo esc_url($primaryCat['url']); ?>"><?php echo $primaryCat['title']; ?></a>
			</div>
			<?php
				}
			?>
			<?php
				//Get post view counter
				$post_view = grandmagazine_get_post_view($post->ID);
				
				if(!empty($post_view))
				{
			?>
			
			<?php
				}
			?>
	    	<div class="title-node"><h1 <?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>class ="withtopbar"<?php } ?>><?php echo esc_html($page_title); ?></h1></div>
	    	  <!--<div class="avatar"><span class="gravatar">
	    	  	<?php $author_id = get_the_author_meta('ID', $post->post_author);
	    	  	 echo get_avatar($author_id, '60' ); ?>
	    	  	</span></div>-->
			  
			  <div class="autor">
			  	<span class="post_info_author">
				  	<?php
				  	$autor_old = get_post_meta(get_the_ID(), 'autor_pg', true);
			  		if(empty($autor_old)){
			  			$author_id = get_the_author_meta('ID', $post->post_author);
			  			$author_name = get_the_author_meta('display_name', $author_id);
			  			$author_nice_name = get_the_author_meta('user_nicename', $author_id);
			  		}else{
			  			$author_name = $autor_old;
			  		}
				  	?>
				  	<?php echo esc_html($author_name); ?>
				 </span>
			  </div>
			  <div class="date">  
				<span class="post_info_date">
				  	<span>
				  		<?php echo get_the_date( 'd F Y' ); ?>
				  	</span>
				</span>
			  </div>
			  <div class="post_info_cat mobile">
					<!--<a href="#"><?php //echo esc_html('Opinión'); ?> </a>-->
				    <?php
				    	$i = 0;
				    	$len = count($post_categories);
				        foreach($post_categories as $c)
				        {
				        	$cat = get_category( $c );
					    }
					?>
					<a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
				</div>
			  <div class="excerpt">
				<?php  
				    the_excerpt();
				?>
			  </div>
			  <div class="inverse">
			    <?php 
				    //Share social media
				    get_template_part("/templates/single_post/template-share-social-media");
				?>
			  </div>
			</div>
	    </div>
	    
	</div>
	<br class="clear"/>
</div>

<!-- Begin content -->
<?php
	$grandmagazine_page_content_class = grandmagazine_get_page_content_class();
?>
<div id="page_content_wrapper" class="<?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?><?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>withtopbar <?php } ?><?php if(!empty($grandmagazine_page_content_class)) { echo esc_attr($grandmagazine_page_content_class); } ?>">