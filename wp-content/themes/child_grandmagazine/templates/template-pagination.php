<?php
	 //Check if advacned pagination
	 $tg_blog_advanced_pagination = kirki_get_option('tg_blog_advanced_pagination');
	 
	 $tg_blog_pagination_infinite = kirki_get_option('tg_blog_pagination_infinite');
	 
	 if(!isset($paged) OR empty($paged))
	 {
	 	$paged = 1;
	 }
	 
	 if(!empty($tg_blog_advanced_pagination) && is_front_page())
	 {
		 $grandmagazine_blog_layout = grandmagazine_get_blog_layout();
?>
<div id="loading_wrapper"><div class="loading_indicator"></div></div>
<br class="clear"/>
<div class="loading_button_wrapper <?php echo esc_attr($grandmagazine_blog_layout); ?>">
	<a id="load_more_button" class="button" href="javascript:;" data-layout="<?php echo esc_attr($grandmagazine_blog_layout); ?>" data-page="<?php echo intval($paged+1); ?>"><?php echo esc_html_e( 'Ver más', 'grandmagazine' ); ?></a>
</div>
<script>
jQuery(document).ready(function(){ 
	"use strict";
	
	jQuery('#load_more_button').on( 'click', function() {
		var blogLayout = jQuery(this).attr('data-layout');
		var viewPosts = jQuery('#post_filter li a.filter_active').attr('data-view');
		var nextPage = parseInt(jQuery(this).attr('data-page'));
		
		if(!jQuery('#loading_wrapper').hasClass('loading') && !jQuery('#post_filterable_wrapper_scroll').hasClass('done'))
		{
			jQuery('.loading_button_wrapper').hide();
			jQuery('#loading_wrapper').addClass('paginating');
			jQuery('#loading_wrapper').addClass('loading');
			
			jQuery.ajax({
			    type: 'POST',
			    url: '<?php echo admin_url('admin-ajax.php'); ?>',
			    data: 'action=grandmagazine_get_post_'+blogLayout+'&paged='+nextPage+'&view='+viewPosts,
			    success: function(results){
				    if(results != '')
				    {
					    jQuery('#post_filterable_wrapper_scroll').append(results);
					    jQuery('#load_more_button').attr('data-page', parseInt(nextPage+1));
					    jQuery('.loading_button_wrapper').show();
					    jQuery('#post_filterable_wrapper_scroll').removeClass('done');
					    
					    <?php
							if(!empty($tg_blog_pagination_infinite))
							{
						?>
						/*setTimeout(function(){
							jQuery('#load_more_button').waypoint(function(direction) {
							    jQuery(this).trigger('click');
							} , { offset: '150%' });
						}, 2000);*/
						<?php
							}
						?>
					    }
					    else
					    {
						    jQuery('#post_filterable_wrapper_scroll').addClass('done');
						    jQuery('#post_filterable_wrapper_scroll').append('<div class="loaded_no_results"><?php echo esc_html__('¡Todas las publicaciones ya están cargadas!', 'grandmagazine' ); ?></div>');
					    }
					    
				    	jQuery('#loading_wrapper').removeClass('loading');
				    	jQuery('#loading_wrapper').removeClass('paginating');
				    	
				    	if(jQuery('#tg_sidebar_sticky').val()==1)
						{
				    		jQuery(window).scrollTop(jQuery(window).scrollTop()+1);
				    	}
				    }
				});
			}
		});
		
		<?php
			if(!empty($tg_blog_pagination_infinite))
			{
		?>
		jQuery('#load_more_button').waypoint(function(direction) {
		    //jQuery(this).trigger('click');
		} , { offset: '100%' });
		<?php
			}
		?>
	});
	</script>
	<?php 
	 	}
	 	else
		{
	?>
	
	<div class="pagination"><div class="pagination_page"><?php echo esc_html($paged); ?></div><?php posts_nav_link(' ', '<i class="fa fa-angle-double-left"></i>'.esc_html__('Newer Posts', 'grandmagazine' ), esc_html__('Older Posts', 'grandmagazine' ).'<i class="fa fa-angle-double-right"></i>'); ?></div>
	<?php
		 }
	?>