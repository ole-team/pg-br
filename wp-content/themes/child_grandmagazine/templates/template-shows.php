<div class="contenedor_shows">
	<div class="contenedor">
		<img class="logo_pg_shows" src="https://static-br.playgroundweb.com/wp-content/uploads/2022/04/playground_presents_br.svg" alt="<?php esc_attr(get_bloginfo('name')); ?>" />
		<?php
				/*<div class="div_shows">
			grandmagazine_filter_shows($items = 4, $echo = TRUE, $show_thumb = TRUE) ;
			?>
		</div>*/?>
		<?php 
		$the_query = new WP_Query(array('category_name' => 'videos','tag' => 'playground-presenta','posts_per_page' => '1') );
		if ( $the_query->have_posts() ) :
			while ( $the_query->have_posts() ) : $the_query->the_post();
			echo '<div class="wrap_box_video"><a href="'.get_the_permalink().'"><div class="icon_play_box"><img src="'.get_stylesheet_directory_uri().'/img/boton-de-play.svg"/></div>';
		       	if ( has_post_thumbnail() ) { 
					$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );
					echo '<img src="'.$image[0].'" />';
				} 
		        echo '<div class="tt_box_video"><h5>' . get_the_title() . '</h5></div>';
		    echo '</a></div>';
			endwhile;
		else : ?>
		<div class="div_videos">
			<div class="wrap_video">
			<?php echo do_shortcode('[pg_dmplayer type="originals"]'); ?>
			</div>
		</div>
		<?php endif; 
		wp_reset_postdata();
		?>
	</div>
</div>