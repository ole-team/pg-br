<?php
//Get page ID
if(is_object($post))
{
    $obj_page = get_page($post->ID);
}
$current_page_id = '';

if(isset($obj_page->ID))
{
    $current_page_id = $obj_page->ID;
}
elseif(is_home())
{
    $current_page_id = get_option('page_on_front');
}
?>

<?php echo do_shortcode(grandmagazine_get_ads('pp_ads_global_before_menu')); ?>
<?php //if (is_home()){ ?>
	
<?php /* } else{ ?>
	<div class="header_ads">
		<div class="ad-160x600 left">
			<!-- 160x600 Izquierda  -->
			<div id='div-gpt-ad-1581543864960-0'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1581543864960-0'); });
				</script>
			</div>
		</div>
		<div class="ad-1024x90">
			<!-- /4923229/pg_megabanner_atf Top Megabanner -->
			<div id='div-gpt-ad-1581543661663-0'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1581543661663-0'); });
				</script>
			</div>
		</div>
		<div class="ad-160x600 right">
			<!-- 160x600 Derecha -->
			<div id='div-gpt-ad-1581543933465-0'>
				<script>
					googletag.cmd.push(function() { googletag.display('div-gpt-ad-1581543933465-0'); });
				</script>
			</div> 
		</div>
	</div>
<?php 
} */ ?>

<div class="header_style_wrapper">
<?php
    //Check if display top bar
    $tg_topbar = kirki_get_option('tg_topbar');
    if(GRANDMAGAZINE_THEMEDEMO && isset($_GET['topbar']) && !empty($_GET['topbar']))
	{
	    $tg_topbar = true;
	}
    
    $grandmagazine_topbar = grandmagazine_get_topbar();
    grandmagazine_set_topbar($tg_topbar);
    
    if(!empty($tg_topbar))
    {
?>

<!-- Begin top bar -->
<div class="above_top_bar">
    <div class="page_content_wrapper">
    
    <div class="top_contact_info">
		<?php
		    $tg_menu_contact_hours = kirki_get_option('tg_menu_contact_hours');
		    
		    if(!empty($tg_menu_contact_hours))
		    {	
		?>
		    <span id="top_contact_hours"><i class="fa fa-clock-o"></i><?php echo esc_html($tg_menu_contact_hours); ?></span>
		<?php
		    }
		?>
		<?php
		    //Display top contact info
		    $tg_menu_contact_number = kirki_get_option('tg_menu_contact_number');
		    
		    if(!empty($tg_menu_contact_number))
		    {
		?>
		    <span id="top_contact_number"><a href="tel:<?php echo esc_attr($tg_menu_contact_number); ?>"><i class="fa fa-phone"></i><?php echo esc_html($tg_menu_contact_number); ?></a></span>
		<?php
		    }
		?>
    </div>
    	
    <?php
    	//Display Top Menu
    	if ( has_nav_menu( 'top-menu' ) ) 
		{
		    wp_nav_menu( 
		        	array( 
		        		'menu_id'			=> 'top_menu',
		        		'menu_class'		=> 'top_nav',
		        		'theme_location' 	=> 'top-menu',
		        	) 
		    ); 
		}
    ?>
    <br class="clear"/>
    </div>
</div>
<?php
    }
?>
<!-- End top bar -->

<?php
    $pp_page_bg = '';
    //Get page featured image
    if(has_post_thumbnail($current_page_id, 'full'))
    {
        $image_id = get_post_thumbnail_id($current_page_id); 
        $image_thumb = wp_get_attachment_image_src($image_id, 'full', true);
        $pp_page_bg = $image_thumb[0];
    }
    
   if(!empty($pp_page_bg) && basename($pp_page_bg)=='default.png')
    {
    	$pp_page_bg = '';
    }
?>
<div class="top_bar">
	<div class="standard_wrapper">
		<!-- Begin logo -->
		<div id="logo_wrapper">
		
		<?php
		    //get custom logo
		    $tg_retina_logo = kirki_get_option('tg_retina_logo');
		
		    if(!empty($tg_retina_logo))
		    {	
		    	//Get image width and height
		    	$image_id = grandmagazine_get_image_id($tg_retina_logo);
		    	$obj_image = wp_get_attachment_image_src($image_id, 'original');
		    	$image_width = 0;
		    	$image_height = 0;
		    	
		    	if(isset($obj_image[1]))
		    	{
		    		$image_width = intval($obj_image[1]/2);
		    	}
		    	if(isset($obj_image[2]))
		    	{
		    		$image_height = intval($obj_image[2]/2);
		    	}
		?>
		<div id="logo_normal" class="logo_container">
		    <div class="logo_align">
		        <a id="custom_logo" class="logo_wrapper" href="<?php echo esc_url(home_url('/')); ?>">
		        	<?php
		    			if($image_width > 0 && $image_height > 0)
		    			{
		    		?>
		    		<img src="<?php echo esc_url($tg_retina_logo); ?>" alt="<?php esc_attr(get_bloginfo('name')); ?>" width="<?php echo esc_attr($image_width); ?>" height="<?php echo esc_attr($image_height); ?>"/>
		    		<?php
		    			}
		    			else
		    			{
		    		?>
		        	<img src="<?php echo esc_url($tg_retina_logo); ?>" alt="<?php esc_attr(get_bloginfo('name')); ?>" width="94" height="44"/>
		        	<?php 
		    	    	}
		    	    ?>
		        </a>
		    </div>
		</div>
		<?php
		    }
		?>
		
		<?php
    		//get custom logo transparent
    	    $tg_retina_transparent_logo = kirki_get_option('tg_retina_transparent_logo');

    	    if(!empty($tg_retina_transparent_logo))
    	    {
    	    	//Get image width and height
		    	$image_id = grandmagazine_get_image_id($tg_retina_transparent_logo);
		    	$obj_image = wp_get_attachment_image_src($image_id, 'original');
		    	$image_width = 0;
		    	$image_height = 0;
		    	
		    	if(isset($obj_image[1]))
		    	{
		    		$image_width = intval($obj_image[1]/2);
		    	}
		    	if(isset($obj_image[2]))
		    	{
		    		$image_height = intval($obj_image[2]/2);
		    	}
    	?>
    	<div id="logo_transparent" class="logo_container">
    		<div class="logo_align">
	    	    <a id="custom_logo_transparent" class="logo_wrapper" href="<?php echo esc_url(home_url('/')); ?>">
	    	    	<?php
						if($image_width > 0 && $image_height > 0)
						{
					?>
					<img src="<?php echo esc_url($tg_retina_transparent_logo); ?>" alt="<?php esc_attr(get_bloginfo('name')); ?>" width="<?php echo esc_attr($image_width); ?>" height="<?php echo esc_attr($image_height); ?>"/>
					<?php
						}
						else
						{
					?>
	    	    	<img src="<?php echo esc_url($tg_retina_transparent_logo); ?>" alt="<?php esc_attr(get_bloginfo('name')); ?>" width="94" height="44"/>
	    	    	<?php 
		    	    	}
		    	    ?>
	    	    </a>
    		</div>
    	</div>
    	<?php
    	    }
    	?>
		<!-- End logo -->
		</div>
		
		<div id="menu_wrapper">
			<div id="logo_left_button">
				<a href="javascript:;" id="mobile_nav_icon">
					<!-- <i class="fa fa-bars"></i> -->
					<div class="menu"></div>
					<span class="logo_right_title"><?php esc_html_e('Menu', 'grandmagazine' ); ?></span>
				</a>
			</div>
	        <div id="nav_wrapper">
	        	<div class="nav_wrapper_inner">
	        		<div id="menu_border_wrapper">
	        			<?php 	
	        				//Check if has custom menu
	        				if ( has_nav_menu( 'primary-menu' ) ) 
	    					{
	    		    		    wp_nav_menu( 
	    		    		        	array( 
	    		    		        		'menu_id'			=> 'main_menu',
	    		    		        		'menu_class'		=> 'nav',
	    		    		        		'theme_location' 	=> 'primary-menu',
	    		    		        		'walker' => new Grand_Magazine_walker(),
	    		    		        	) 
	    		    		    ); 
	    		    		}
	        			?>
	        		</div>
	        	</div>
	        </div>
	        <!-- End main nav -->
        
        <!-- Begin right corner buttons -->
    	<div id="logo_right_button">
	    	<?php
		    	$tg_browse_menu = kirki_get_option('tg_browse_menu');
		    	
		    	if(!empty($tg_browse_menu))
		    	{
		    ?>
		    	<a href="javascript:;" id="browse_icon">
					<i class="fa fa-newspaper-o"></i>
					<span class="logo_right_title"><?php esc_html_e('Browse', 'grandmagazine' ); ?></span>
				</a>
			<?php
				}
			?>
	    	<?php
		    	$tg_search_menu = kirki_get_option('tg_search_menu');
		    	
		    	if(!empty($tg_search_menu))
		    	{
		    ?>
			<a href="javascript:;" id="search_icon">
				<!-- <i class="fa fa-search"></i> -->
				<div class="search"></div>
				<span class="logo_right_title"><?php esc_html_e('Busca', 'grandmagazine' ); ?></span>
			</a>
			<?php
				}
			?>
			
    	</div>
    	<!-- End right corner buttons -->
        </div>
    	</div>
    </div>
</div>

<!-- Begin search content -->
<div id="search_wrapper" class="overlay_background">
	<a id="close_search" href="javascript:;"><i class="fa fa-close"></i></a>
	<div class="overlay_background_wrapper">
		<div class="overlay_background_content">
			<div class="overlay_background_content_inner">
				<form role="search" method="get" name="searchform" id="searchform" action="<?php echo esc_url(home_url('/')); ?>/">
			        <input type="text" value="<?php the_search_query(); ?>" name="s" id="s" autocomplete="off" placeholder="<?php esc_html_e( 'Search', 'grandmagazine' ); ?>"/>
			        <div id="search_loading_wrapper"><div class="loading_indicator"></div></div>
			        <div id="autocomplete" class="visible">
			        	<?php
			        		$category_id = get_cat_ID('Video');
							grandmagazine_cat_posts_search($cat_id = $category_id, $items = 5, $echo = TRUE, $show_thumb = TRUE) ;
						?>
			        </div>
			        <div class="share_social">
						<a class="facebook" target="_blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
						<a class="twitter" target="_blank" href="https://twitter.com/"><i class="fa fa-twitter"></i></a>
						<a class="instagram" target="_blank" href="https://instagram.com/"><i class="fa fa-instagram"></i></a>
						<a class="youtube" target="_blank" href="https://youtube.com/"><i class="fa fa-youtube"></i></a>
					</div>
			    </form>
			</div>
		</div>
	</div>
</div>
<!-- End search content -->

<!-- Begin browse content -->
<div id="browse_wrapper" class="overlay_background">
	<a id="close_browse" href="javascript:;"><i class="fa fa-close"></i></a>
	<div class="overlay_background_wrapper">
		<div class="overlay_background_content">
			<div class="overlay_background_content_inner">
				<div id="browse_category">
					<?php
						$has_category = false;
						$categories_arr = get_categories();
						
						if(function_exists('z_taxonomy_image_url'))
						{
							foreach($categories_arr as $category_arr) 
							{
								if(function_exists('z_taxonomy_image_url'))
								{
									$category_id = $category_arr->term_id;
								    $category_image = z_taxonomy_image_url($category_id);
								    
								    if(!empty($category_image))
									{
										$has_category = true;
									}
								}
							}
						}
						
						if($has_category)
						{
					?>
					<h2><?php echo esc_html_e( 'Browse', 'grandmagazine' ); ?></h2>
					<div class="page_tagline"><?php echo esc_html_e( 'News collects all the stories you want to read', 'grandmagazine' ); ?></div>
					<ul class="browse_category_wrapper">
						<?php
							foreach($categories_arr as $category_arr) 
							{
								$category_id = $category_arr->term_id;
								$category_link = get_category_link($category_id);
								$category_name = $category_arr->name;
								$category_image = '';
								
								if(function_exists('z_taxonomy_image_url'))
								{
									$category_image = z_taxonomy_image_url($category_id);
								}
								
								if(!empty($category_image))
								{
									$image_id = grandmagazine_get_image_id($category_image);
									$obj_image = wp_get_attachment_image_src($image_id, 'thumbnail');
						?>
								<li class="one_sixth">
									<a href="<?php echo esc_url($category_link); ?>">
										<img src="<?php echo esc_url($obj_image[0]); ?>" alt="<?php echo esc_html($category_name); ?>"/>
										<div class="browse_category_name">
											<?php echo esc_html($category_name); ?>
										</div>
									</a>
								</li>
						<?php
								}
							}
						?>
					</ul>
					<?php
						}
					?>
				</div>
				<?php
					$tg_browse_menu_editors = kirki_get_option('tg_browse_menu_editors');
					
					if(!is_array($tg_browse_menu_editors))
					{
						$tg_browse_menu_editors = array('Administrator', 'Author', 'Editor');
					}
					
					// prepare arguments
					$args  = array(
						// search only for Authors role
						'role__in' => $tg_browse_menu_editors,
						// order results by display_name
						'orderby' => 'display_name'
					);
					
					// Create the WP_User_Query object
					$wp_user_query = new WP_User_Query($args);
					
					// Get the results
					$authors_arr = $wp_user_query->get_results();
					
					// Check for results
					if(!empty($authors_arr))
					{
						if(!empty($categories_arr))
						{
				?>
						<br class="clear"/><br/>
				<?php
						}
				?>
				<div id="browse_author">
					<h3><?php echo esc_html_e( 'Editors', 'grandmagazine' ); ?></h3>
					<ul class="browse_author_wrapper">
						<?php
							foreach ($authors_arr as $author)
							{
								$author_info = get_userdata($author->ID);
						?>
							<li class="one_sixth">
								<a href="<?php echo get_author_posts_url($author->ID, get_the_author_meta('nice_name') ); ?>">
									<span class="gravatar"><?php echo get_avatar($author->ID, '150' ); ?></span>
									<div class="display_name"><?php echo esc_html($author->display_name); ?></div>
								</a>
							</li>
						<?php
							}
						?>
					</ul>
				</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</div>
<!-- End browse content -->