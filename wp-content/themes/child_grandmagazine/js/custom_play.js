var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};	

// The checker
const pgElementIsInView = el => {
	const scroll = window.scrollY || window.pageYOffset
	const boundsTop = el.getBoundingClientRect().top + scroll
	
	const viewport = {
		top: scroll,
		bottom: scroll + window.innerHeight,
	}
	
    const bounds = {
		top: boundsTop,
		bottom: boundsTop + el.clientHeight,
	}
	
	return ( bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom ) 
		|| ( bounds.top <= viewport.bottom && bounds.top >= viewport.top );
}


// Usage.
document.addEventListener( 'DOMContentLoaded', () => {
	adSP_tags = false;
	getJSON('https://us-central1-playground-277821.cloudfunctions.net/playground-geoprod',function(err, data) {
	  if (err !== null) {
	    //alert('Something went wrong: ' + err);
	  } else {
	  	console.log(data.country);
	    if(data.country == 'ES'){
	    	var body = document.getElementsByTagName("body")[0];
	    	body.classList.add("ad_spain");
	    	adSP_tags = true;
	    }
	  }
	});
	const headerElement = document.querySelector( '.header_style_wrapper' )
	const elmnt = document.getElementById('ad_spain_top');
	var wi = window.innerWidth;
	validate = document.querySelector("body");
	var useAD_SPN = validate.classList.contains("ad_spain");
	if(useAD_SPN){
		googletag.cmd.push(function() { 
			googletag.display('pg_megabanner');
			if (wi > 1220) {
				googletag.display('pg_sky_izq');
				googletag.display('pg_sky_der');
			}
		});
		googletag.pubads().addEventListener('slotRenderEnded', function(event) {
		  handler();
		});
	}

	const handler = () => raf( () => {
		if(wi>760){
		const altEm = elmnt.offsetHeight;
		headerElement.style.top = altEm+'px';
		scroll = window.scrollY || window.pageYOffset;
		if(scroll >= altEm){
			newAlt = 0;
		}else{
			newAlt = altEm - scroll;
		}
		headerElement.style.top = newAlt+'px';
		}
	} )
	
	handler()
	window.addEventListener( 'scroll', handler )
} )

// requestAnimationFrame
const raf = 
    window.requestAnimationFrame ||
    window.webkitRequestAnimationFrame ||
    window.mozRequestAnimationFrame ||
    function( callback ) {
        window.setTimeout( callback, 1000 / 60 )
    }