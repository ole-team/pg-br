<?php
/**
 * The main template file for display single post page.
 *
 * @package WordPress
*/

get_header(); 

$grandmagazine_topbar = grandmagazine_get_topbar();

/**
*	Get current page id
**/

$current_page_id = $post->ID;

/**
*	Get current page id
**/

$current_page_id = $post->ID;

//Include custom header feature
get_template_part("/templates/single_post/template-post-header-minimal");
?>

<div class="inner">

	<!-- Begin main content -->
	<div class="inner_wrapper">

		<div class="sidebar_content full_width">
					
<?php
if (have_posts()) : while (have_posts()) : the_post();
?>
						
<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<?php
	    		//Get post featured content
			    get_template_part("/templates/single_post/template-post-featured-content-minimal");

				the_content();
				wp_link_pages();
				
				//Get ads after content
				echo do_shortcode(grandmagazine_get_ads('pp_ads_single_after_content'));

				//Get post tags
			    get_template_part("/templates/single_post/template-post-tags");
			?>
			
			<br class="clear"/>
			<?php
				//Get post author
			    get_template_part("/templates/single_post/template-post-author");
			    
			    //Get post related
			    get_template_part("/templates/single_post/template-post-related");
			    
			    //Get post more from category
			    get_template_part("/templates/single_post/template-post-more-from");
			    
			    //Get editor picks posts
			    get_template_part("/templates/single_post/template-post-editor-picks");
			?>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->

<div class="fullwidth_comment_wrapper sidebar">
	<?php comments_template( '', true ); ?>
</div>

<?php endwhile; endif; ?>
						
    	</div>
    
    </div>
    <!-- End main content -->
   
</div>

<br class="clear"/>

</div>

<?php get_footer(); ?>