<?php
/**
 * The main template file for display blog page.
 *
 * @package WordPress
*/

/**
*	Get Current page object
**/
if(!is_null($post))
{
	$page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*	Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 

$is_standard_wp_post = FALSE;

if(is_tag())
{
    $is_standard_wp_post = TRUE;
} 
elseif(is_category())
{
    $is_standard_wp_post = TRUE;
}
elseif(is_archive())
{
    $is_standard_wp_post = TRUE;
} 
		
get_header(); 

//Include post featured section
get_template_part("/templates/template-featured-posts");


if(is_category() OR is_tag() OR is_archive() OR is_search())
{
	get_template_part("/templates/template-header");
}
else
{
?>
<div id="page_content_wrapper">
<?php
}
?>  
    <div class="inner three_cols">

    	<!-- Begin main content -->
    	<div class="inner_wrapper">
	    	
	    		<?php
				 	grandmagazine_set_blog_layout('blog_newspaper');
				 
				 	//Include post filter bar
				 	get_template_part("/templates/template-filter");
				?>

    			<div class="sidebar_content full_width three_cols">
	    			<div id="post_filterable_wrapper" class="three_cols">
<?php
//Include post search bar
get_template_part("/templates/template-search");

$featured_posts = grandmagazine_get_featured_posts();
if(!empty($featured_posts) && is_array($featured_posts))
{
	$query_arr = array(
		'post__not_in' => $featured_posts,
		'paged' => $paged,
		'suppress_filters' => false,
	);
    query_posts($query_arr);
}

$wp_query = grandmagazine_get_wp_query();
$count_all_posts = $wp_query->post_count;

$key = 0;
if (have_posts()) : while (have_posts()) : the_post();
	$image_thumb = '';
	$key++;
	$image_id = get_post_thumbnail_id(get_the_ID());
?>
<div id="post-<?php the_ID(); ?>" <?php post_class('thumbnail'); ?> <?php if($key%3==0) { ?>data-column="last"<?php } ?>>
	<div class="post_wrapper">
	    <div class="post_content_wrapper">
		    <div class="two_third post_header">
			    <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			    <div class="post_detail post_date">
			      <span class="post_info_date">
			      	<span>
			      		<a href="<?php the_permalink(); ?>">
				      		<i class="fa fa-clock-o"></i>
				      		<?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      		</a>
			      	</span>
			      </span>
			      <span class="post_info_comment">
					  <i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
				  </span>
				</div>
		    </div>
		    
		    <div class="one_third last">
			    <?php
					if(has_post_thumbnail(get_the_ID(), 'thumbnail'))
					{
					    $image_id = get_post_thumbnail_id(get_the_ID());
					    $image_thumb = wp_get_attachment_image_src($image_id, 'thumbnail', true);
					}
					
					//Get post featured content
				    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
				    {
				?>
				<div class="post_img thumbnail">
				 	<a href="<?php the_permalink(); ?>">
				 		<img src="<?php echo esc_url($image_thumb[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($image_thumb[1]); ?>px;height:<?php echo esc_attr($image_thumb[2]); ?>px;"/>
				 		
				 		<?php
					 		//Get post trending or hot status
						  	echo grandmagazine_get_status($post->ID);
				    	?>
				    </a>
				</div>
				<?php
					}
				?>
		    </div>
	    </div>
	</div>
</div>
<?php 
	if($key%3==0 OR $key == $count_all_posts) 
	{
		echo '<br class="clear"/>';
	}

	endwhile; endif; 
?>
	    	</div>

	    	<?php
				//Include pagination
				get_template_part("/templates/template-pagination");
	    	?>
    		
			</div>
    		
    	</div>
    <!-- End main content -->
	</div>
</div>
<?php get_footer(); ?>