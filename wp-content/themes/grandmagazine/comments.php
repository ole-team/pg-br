<?php
//Get ads before comment
echo do_shortcode(grandmagazine_get_ads('pp_ads_single_before_comment'));

//Required password to comment
if ( post_password_required() ) { ?>
	<p><?php esc_html_e( 'This post is password protected. Enter the password to view comments.', 'grandmagazine' ); ?></p>
<?php
	return;
}
?>
<?php 
//Display Comments
if( have_comments() ) : ?> 

<h4><?php comments_number(esc_html__( 'Leave A Reply', 'grandmagazine' ), esc_html__( '1 Comment', 'grandmagazine' ), '% '.esc_html__( 'Comments', 'grandmagazine' )); ?></span></h4>

<div>
	<a name="comments"></a>
	<?php wp_list_comments( array('callback' => 'grandmagazine_comment', 'avatar_size' => '40') ); ?>
</div>

<!-- End of thread -->  
<div style="height:10px"></div>

<?php endif; ?> 


<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>

<div class="pagination"><p><?php previous_comments_link('<'); ?> <?php next_comments_link('>'); ?></p></div><br class="clear"/>

<?php endif; // check for comment navigation ?>


<?php 
//Display Comment Form
if ('open' == $post->comment_status) : ?> 

<?php comment_form(); ?>
			
<?php endif; ?>