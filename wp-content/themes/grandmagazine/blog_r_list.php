<?php
/**
 * The main template file for display blog page.
 *
 * @package WordPress
*/

/**
*	Get Current page object
**/
if(!is_null($post))
{
	$page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*	Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

get_header(); 

$is_standard_wp_post = FALSE;
$page_sidebar = 'page-sidebar';

if(is_tag())
{
    $is_standard_wp_post = TRUE;
    $page_sidebar = 'tag-sidebar';
} 
elseif(is_category())
{
    $is_standard_wp_post = TRUE;
    $page_sidebar = 'category-sidebar';
}
elseif(is_archive())
{
    $is_standard_wp_post = TRUE;
    $page_sidebar = 'archives-sidebar';
} 
elseif(is_search())
{
    $is_standard_wp_post = TRUE;
    $page_sidebar = 'search-sidebar';
} 
		
get_header(); 

//Include post featured section
get_template_part("/templates/template-featured-posts");

if(is_category() OR is_tag() OR is_archive() OR is_search())
{
	get_template_part("/templates/template-header");
}
else
{
?>
<div id="page_content_wrapper">
<?php
}
?>
    <div class="inner">

    	<!-- Begin main content -->
    	<div class="inner_wrapper">
	    	
	    		<?php
				 	grandmagazine_set_blog_layout('blog_r_list');
				 
				 	//Include post filter bar
				 	get_template_part("/templates/template-filter");
				?>

    			<div class="sidebar_content">
	    			<div id="post_filterable_wrapper">
<?php
//Include post search bar
get_template_part("/templates/template-search");

$featured_posts = grandmagazine_get_featured_posts();
if(!empty($featured_posts) && is_array($featured_posts))
{
	$query_arr = array(
		'post__not_in' => $featured_posts,
		'paged' => $paged,
		'suppress_filters' => false,
	);
    query_posts($query_arr);
}

if (have_posts()) : while (have_posts()) : the_post();
	$image_thumb = '';
	$image_id = get_post_thumbnail_id(get_the_ID());
?>

<!-- Begin each blog post -->
<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post_wrapper">
	    
	    <div class="post_content_wrapper">
	    
	    	<div class="post_header search">
		    	<?php
				    //Get post featured content
				    $post_content_class = 'one';
				    
				    $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
				    $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
		    		
				    //Get post featured content
				    if(isset($small_image_url[0]) && !empty($small_image_url[0]))
				    {
				        $post_content_class = 'two_third last';
				?>
				
				    <div class="post_img static one_third">
				      	<a href="<?php the_permalink(); ?>">
				      		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
				      		
				      		<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status($post->ID);
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view($post->ID);
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					      	
					     </a>
				     </div>
				
				<?php
				    }
				?>
				
				<div class="post_header_title <?php echo esc_attr($post_content_class); ?>">
					<?php
						//Get Post's Categories
					    $post_categories = wp_get_post_categories($post->ID);
					    if(!empty($post_categories))
					    {
					?>
					<div class="post_info_cat">
					    <?php
					    	$i = 0;
					    	$len = count($post_categories);
					        foreach($post_categories as $c)
					        {
					        	$cat = get_category( $c );
					    ?>
					        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
					    <?php
						    	if(GRANDMAGAZINE_THEMEDEMO)
						    	{
							    	break;
						    	}
						    }
						?>
					</div>
					<?php
						}
					?>
			      	<h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
			      	<div class="post_detail post_date">
			      		<span class="post_info_date">
			      			<span>
			      				<a href="<?php the_permalink(); ?>">
				      				<i class="fa fa-clock-o"></i>
				       				<?php esc_html_e( 'Posted On', 'grandmagazine' ); ?> <?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
			      				</a>
			      			</span>
			      		</span>
			      		<span class="post_info_author">
			      			<?php
			      				$author_name = get_the_author();
			      			?>
			      			<a href="<?php echo get_author_posts_url( get_the_author_meta( 'ID' ), get_the_author_meta( 'user_nicename' ) ); ?>"><span class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '60' ); ?></span><?php echo esc_html($author_name); ?></a>
			      		</span>
			      		<span class="post_info_comment">
					  		<i class="fa fa-comment-o"></i><?php echo get_comments_number($post->ID); ?>
					  	</span>
				  	</div>
				  	<?php echo grandmagazine_get_excerpt_by_id(get_the_ID(), 20); ?>
			   </div>
			</div>
			
	    </div>
	    
	</div>

</div>
<!-- End each blog post -->

<?php endwhile; endif; ?>

	    	</div>

	    	<?php
				//Include pagination
				get_template_part("/templates/template-pagination");
	    	?>
    		
			</div>
    	
    		<div class="sidebar_wrapper">
    		
    			<div class="sidebar">
    			
    				<div class="content">

    					<?php 
						if (is_active_sidebar($page_sidebar)) { ?>
		    	    		<ul class="sidebar_widget">
		    	    			<?php dynamic_sidebar($page_sidebar); ?>
		    	    		</ul>
		    	    	<?php } ?>
    				
    				</div>
    		
    			</div>
    			<br class="clear"/>
    		</div>
    		
    	</div>
    <!-- End main content -->
	</div>
	
</div>
<?php get_footer(); ?>