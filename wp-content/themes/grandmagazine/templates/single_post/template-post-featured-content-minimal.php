<?php
	$tg_blog_feat_content = kirki_get_option('tg_blog_feat_content');
    $post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true); 
	    
    switch($post_ft_type)
    {
    	case 'Image':
    	default:
    		if(has_post_thumbnail(get_the_ID(), 'large'))
        	{
	        	$image_id = get_post_thumbnail_id(get_the_ID());
        		$large_image_url = wp_get_attachment_image_src($image_id, 'original', true);
        		$small_image_url = wp_get_attachment_image_src($image_id, 'large', true);
?>

    	    <div class="post_img featured_content">
    	    	<a href="<?php echo esc_url($large_image_url[0]); ?>" class="img_frame">
    	    		<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
             </a>
    	    </div>

<?php
    		}
    	break;
    	
    	case 'Vimeo Video':
    		$post_ft_vimeo = get_post_meta(get_the_ID(), 'post_ft_vimeo', true);
?>
    		<?php echo do_shortcode('[tg_vimeo video_id="'.$post_ft_vimeo.'" width="670" height="377"]'); ?>
<?php
    	break;
    	
    	case 'Youtube Video':
    		$post_ft_youtube = get_post_meta(get_the_ID(), 'post_ft_youtube', true);
?>
    		<?php echo do_shortcode('[tg_youtube video_id="'.$post_ft_youtube.'" width="670" height="377"]'); ?>
<?php
    	break;
    	
    	case 'Gallery':
    		$post_ft_gallery = get_post_meta(get_the_ID(), 'post_ft_gallery', true);
?>
    		<?php echo do_shortcode('[tg_gallery_slider gallery_id="'.$post_ft_gallery.'" width="670" height="270"]'); ?>
<?php
    	break;
    	
    } //End switch
?>