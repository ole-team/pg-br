<?php
    $tg_blog_display_related = kirki_get_option('tg_blog_display_related');
    
    if($tg_blog_display_related)
    {
	    $tg_blog_display_related_posts_items = kirki_get_option('tg_blog_display_related_posts_items');
	    
	    $single_related_posts = array();
?>

<?php
//for use in the loop, list 9 post titles related to post's tags on current post
$tags = wp_get_post_tags($post->ID);

if ($tags) {

    $tag_in = array();
  	//Get all tags
  	foreach($tags as $tags)
  	{
      	$tag_in[] = $tags->term_id;
  	}

  	$args=array(
      	  'tag__in' => $tag_in,
      	  'post__not_in' => array($post->ID),
      	  'showposts' => $tg_blog_display_related_posts_items,
      	  'ignore_sticky_posts' => 1,
      	  'orderby' => 'date',
      	  'order' => 'DESC',
      	  'suppress_filters' => false,
  	 );
  	$my_query = new WP_Query($args);
  	$i_post = 1;
  	
  	if( $my_query->have_posts() ) {
 ?>
 	<br/>
  	<h5><?php echo esc_html_e( 'You might also like', 'grandmagazine' ); ?></h5><hr/>
  	<div class="post_related">
    <?php
       while ($my_query->have_posts()) : $my_query->the_post();
       
       array_push($single_related_posts, get_the_ID());
       
       $last_class = '';
       if($i_post%3==0)
       {
	       $last_class = 'last';
       }
       
       $image_thumb = '';
					
		if(has_post_thumbnail(get_the_ID(), 'grandmagazine_blog'))
		{
		    $image_id = get_post_thumbnail_id(get_the_ID());
		    $image_thumb = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
		    $image_thumb = grandmagazine_filter_default_featued_image($image_thumb);
		}
    ?>
       <div class="one_third <?php echo esc_attr($last_class); ?>">
		   <!-- Begin each blog post -->
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			
				<div class="post_wrapper grid_layout">
				
					<?php
					    //Get post featured content
						if(!empty($image_thumb))
						{
					       $small_image_url = wp_get_attachment_image_src($image_id, 'grandmagazine_blog', true);
					       $small_image_url = grandmagazine_filter_default_featued_image($small_image_url);
					?>
					
					   <div class="post_img small static">
					       <a href="<?php the_permalink(); ?>">
					       	<img src="<?php echo esc_url($small_image_url[0]); ?>" alt="" class="" style="width:<?php echo esc_attr($small_image_url[1]); ?>px;height:<?php echo esc_attr($small_image_url[2]); ?>px;"/>
					       	
					       	<?php
					      		//Get post trending or hot status
						  		echo grandmagazine_get_status($post->ID);
						  		
					      		//Get post featured content
						  		$post_ft_type = get_post_meta(get_the_ID(), 'post_ft_type', true);	
						  		
						  		switch($post_ft_type)
							    {
							    	case 'Vimeo Video':
							    	case 'Youtube Video':
							?>
										<div class="post_img_type_wrapper">
								    		<i class="fa fa-video-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    	case 'Gallery':
							?>
							    		<div class="post_img_type_wrapper">
								    		<i class="fa fa-camera"></i>
								    	</div>
							<?php
							    	break;
							    	
							    } //End switch
					      	?>
					      	
					      	<?php
								//Get post view counter
								$post_view = grandmagazine_get_post_view($post->ID);
								
								if(!empty($post_view))
								{
							?>
							<div class="post_info_view">
								<i class="fa fa-eye"></i><?php echo esc_html($post_view); ?>
							</div>
							<?php
								}
							?>
					       </a>
					   </div>
					
					<?php
						}
					?>
				    
				    <div class="blog_grid_content">
						<div class="post_header grid">
							<?php
								//Get Post's Categories
							    $post_categories = wp_get_post_categories($post->ID);
							    if(!empty($post_categories))
							    {
							?>
							<div class="post_info_cat">
							    <?php
							    	$i = 0;
							    	$len = count($post_categories);
							        foreach($post_categories as $c)
							        {
							        	$cat = get_category( $c );
							    ?>
							        <a href="<?php echo esc_url(get_category_link($cat->term_id)); ?>"><?php echo esc_html($cat->name); ?></a>
							    <?php
								    	if(GRANDMAGAZINE_THEMEDEMO)
								    	{
									    	break;
								    	}
								    }
								?>
							</div><br/>
							<?php
								}
							?>
						    <strong><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></strong>
						    <div class="post_detail post_date">
					      		<span class="post_info_date">
					      			<span>
					      				<a href="<?php the_permalink(); ?>">
						      				<i class="fa fa-clock-o"></i>
						       				<?php echo date_i18n(GRANDMAGAZINE_THEMEDATEFORMAT, get_the_time('U')); ?>
					      				</a>
					      			</span>
					      		</span>
						  	</div>
						</div>
				    </div>
				    
				</div>
			
			</div>
			<!-- End each blog post -->
       </div>
     <?php
     	$i_post++;
       	endwhile;
       	
       	wp_reset_postdata();
       	
       	grandmagazine_set_single_related_posts($single_related_posts);
     ?>
  	</div>
  	<br class="clear"/>
<?php
  	}
}
?>

<?php
    } //end if show related
    
    //Get ads after related
	echo do_shortcode(grandmagazine_get_ads('pp_ads_single_after_related'));
?>