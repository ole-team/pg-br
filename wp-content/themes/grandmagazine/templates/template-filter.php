<?php
if(is_front_page())
{
	//Check if display post filterable
	$tg_blog_filter = kirki_get_option('tg_blog_filter');
	
	if(!empty($tg_blog_filter))
	{
		//Check if using advanced pagination
		$tg_blog_advanced_pagination = kirki_get_option('tg_blog_advanced_pagination');
		$tg_blog_editor_picks_cat = kirki_get_option('tg_blog_editor_picks_cat');
		
		if(empty($tg_blog_advanced_pagination))
		{
			$base_link_url = home_url();
			
			$trending_url = $base_link_url.'/?view=trending';
			$hot_url = $base_link_url.'/?view=hot';
			
			$editor_picks_url = $base_link_url.'/?view='.$tg_blog_editor_picks_cat;
		}
		else
		{
			$grandmagazine_blog_layout = grandmagazine_get_blog_layout();
			if(empty($grandmagazine_blog_layout))
			{
				$grandmagazine_blog_layout = 'blog_r';
			}
			
			$base_link_url = 'javascript:;';
			$trending_url = 'javascript:;';
			$hot_url = 'javascript:;';
			$editor_picks_url = 'javascript:;';
		}
?>
<div class="post_filter_wrapper">
	<h2><?php echo date_i18n("l"); ?><br/><?php echo date_i18n("F j"); ?></h2>
	<ul id="post_filter" data-layout="<?php echo esc_attr($grandmagazine_blog_layout); ?>">
		<li>
			<a data-view="latest" title="<?php esc_html_e( "Latest", 'grandmagazine' ); ?>" href="<?php echo esc_attr($base_link_url); ?>" <?php if(!isset($_GET['view']) OR empty($_GET['view'])) { ?>class="filter_active"<?php } ?>>
				<i class="fa fa-clock-o"></i><?php esc_html_e( "Latest", 'grandmagazine' ); ?>
			</a>
		</li>
		
		<li>
			<a data-view="trending" title="<?php esc_html_e( "Trending", 'grandmagazine' ); ?>" href="<?php echo esc_attr($trending_url); ?>" <?php if(isset($_GET['view']) && $_GET['view'] == 'trending') { ?>class="filter_active"<?php } ?>>
				<i class="fa fa-flash"></i><?php esc_html_e( "Trending", 'grandmagazine' ); ?>
			</a>
		</li>
		
		<li>
			<a data-view="hot" title="<?php esc_html_e( "Hot", 'grandmagazine' ); ?>" href="<?php echo esc_attr($hot_url); ?>" <?php if(isset($_GET['view']) && $_GET['view'] == 'hot') { ?>class="filter_active"<?php } ?>>
				<i class="fa fa-fire"></i><?php esc_html_e( "Hot", 'grandmagazine' ); ?>
			</a>
		</li>
		<?php
			if(!empty($tg_blog_editor_picks_cat))
			{
		?>
		<li>
			<a data-view="editors_picks" title="<?php esc_html_e( "Editor Picks", 'grandmagazine' ); ?>" href="<?php echo esc_attr($editor_picks_url); ?>" <?php if(isset($_GET['view']) && $_GET['view'] == $tg_blog_editor_picks_cat) { ?>class="filter_active"<?php } ?>>
				<i class="fa fa-heart"></i><?php esc_html_e( "Editor Picks", 'grandmagazine' ); ?>
			</a>
		</li>
		<?php
			}
		?>
	</ul>
	<br class="clear"/>
</div>
<?php
	if(!empty($tg_blog_advanced_pagination))
	{
?>
<script>
jQuery(document).ready(function(){ 
	"use strict";
	
	jQuery('#post_filter li a').on( 'click', function() {
		jQuery('#post_filter li a').removeClass('filter_active');
		jQuery(this).addClass('filter_active');
		
		jQuery('#loading_wrapper').addClass('loading');
		var blogLayout = jQuery('#post_filter').attr('data-layout');
		var viewPosts = jQuery(this).attr('data-view');

		jQuery.ajax({
		    type: 'POST',
		    url: '<?php echo admin_url('admin-ajax.php'); ?>',
		    data: 'action=grandmagazine_get_post_'+blogLayout+'&paged=1&view='+viewPosts,
		    success: function(results){
			    
			    if(results != '')
			    {
				    jQuery('#post_filterable_wrapper').html(results);
				    jQuery('#load_more_button').attr('data-page', 2);
				    jQuery('#load_more_button').show();
				    jQuery('#post_filterable_wrapper').removeClass('done');
				    
					<?php
						//Check if smart sticky menu
						$tg_smart_fixed_menu = kirki_get_option('tg_smart_fixed_menu');
						
						if(!empty($tg_smart_fixed_menu))
						{
					?>
					var offset = 40;
					if(jQuery(window).width() > 480 && jQuery(window).width() <= 768)
					{
						offset = 120;
					}
					else jQuery(window).width() <= 480
					{
						offset = 190;
					}
				    var scrollToPos = parseInt(jQuery('#post_filter').offset().top-offset);
				    <?php
					    }
					    else
					    {
					?>
					var offset = 140;
					if(jQuery(window).width() > 480 && jQuery(window).width() <= 768)
					{
						offset = 120;
					}
					else jQuery(window).width() <= 480
					{
						offset = 190;
					}
					var scrollToPos = parseInt(jQuery('#post_filter').offset().top-140);
					<?php
						}
					?>

				    jQuery('body,html').animate({
						scrollTop: scrollToPos
					}, 300);
					
					<?php
						$tg_blog_pagination_infinite = kirki_get_option('tg_blog_pagination_infinite');
						
						if(!empty($tg_blog_pagination_infinite))
						{
					?>
					setTimeout(function(){
						jQuery('#load_more_button').waypoint(function(direction) {
						    jQuery(this).trigger('click');
						} , { offset: '80%' });
					}, 2000);
					<?php
						}
					?>
			    }
			    else
			    {
				    jQuery('#post_filterable_wrapper').addClass('done');
				    jQuery('#post_filterable_wrapper').html('<div class="loaded_no_results"><?php echo esc_html__('All posts are already loaded!', 'grandmagazine' ); ?></div>');
			    }
			    jQuery('.loading_button_wrapper').show();
		    	jQuery('#loading_wrapper').removeClass('loading');
		    }
		});
	});
});
</script>
<?php
	}
?>

<?php
	}
}
?>