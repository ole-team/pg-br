<?php
    $image_id = get_post_thumbnail_id(get_the_ID());
    $pin_thumb = wp_get_attachment_image_src($image_id, 'grandmagazine_blog_thumb', true);
    
    if(!isset($pin_thumb[0]))
    {
	    $pin_thumb[0] = '';
    }
?>
<h2><?php echo esc_html_e( 'Share', 'grandmagazine' ); ?></h2>
<div class="page_tagline"><?php echo esc_html_e( 'Share stories you like to your friends', 'grandmagazine' ); ?></div>
<div id="social_share_wrapper">
	<ul>
		<li><a class="facebook" title="<?php esc_html_e( 'Share On Facebook', 'grandmagazine' ); ?>" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>"><i class="fa fa-facebook"></i></a></li>
		<li><a class="twitter" title="<?php esc_html_e( 'Share On Twitter', 'grandmagazine' ); ?>" target="_blank" href="https://twitter.com/intent/tweet?original_referer=<?php echo get_permalink(); ?>&url=<?php echo get_permalink(); ?>"><i class="fa fa-twitter"></i></a></li>
		<li><a class="pinterest" title="<?php esc_html_e( 'Share On Pinterest', 'grandmagazine' ); ?>" target="_blank" href="http://www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&media=<?php echo urlencode($pin_thumb[0]); ?>"><i class="fa fa-pinterest"></i></a></li>
		<li><a class="google" title="<?php esc_html_e( 'Share On Google+', 'grandmagazine' ); ?>" target="_blank" href="https://plus.google.com/share?url=<?php echo get_permalink(); ?>"><i class="fa fa-google-plus"></i></a></li>
		<li><a class="mail" title="<?php esc_html_e('Share by Email', 'grandmagazine' ); ?>" href="mailto:someone@example.com?Subject=<?php echo rawurlencode($post->post_title); ?>&amp;Body=<?php echo rawurlencode(get_permalink($post->ID)); ?>"><i class="fa fa-envelope"></i></a></li>
	</ul>
</div>