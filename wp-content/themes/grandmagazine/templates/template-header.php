<?php
/**
*	Get Current page object
**/
if(!is_null($post))
{
	$page_obj = get_page($post->ID);
}

$current_page_id = '';

/**
*	Get current page id
**/

if(!is_null($post) && isset($page_obj->ID))
{
    $current_page_id = $page_obj->ID;
}

//Get page header display setting
$page_title = get_the_title();
$page_show_title = get_post_meta($current_page_id, 'page_show_title', true);

if(is_tag())
{
	$page_show_title = 0;
	$page_title = single_cat_title( '', false );
	$term = 'tag';
} 
elseif(is_category())
{
    $page_show_title = 0;
	$page_title = single_cat_title( '', false );
	$term = 'category';
}
elseif(is_author())
{
	$page_show_title = 0;
	$author_name = get_the_author();
	
	$page_title = $author_name;
	$page_tagline = esc_html__('All blog posts from', 'grandmagazine').' '.$page_title;
	$term = 'archive';
}
elseif(is_archive())
{
	$page_show_title = 0;

	if ( is_day() ) : 
		$page_title = get_the_date(); 
    elseif ( is_month() ) : 
    	$page_title = get_the_date('F Y'); 
    elseif ( is_year() ) : 
    	$page_title = get_the_date('Y'); 
    elseif ( !empty($term) ) : 
    	$ob_term = get_term_by('slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    	$page_taxonomy = get_taxonomy($ob_term->taxonomy);
    	$page_title = $page_taxonomy->labels->name.' "'.$ob_term->name.'"';
    else :
    	$page_title = esc_html__('Blog Archives', 'grandmagazine'); 
    endif;
    
    $page_tagline = esc_html__('All blog posts from', 'grandmagazine').' '.$page_title;
    $term = 'archive';
    
}
else if(is_search())
{
	$page_show_title = 0;
	$page_title = esc_html__('Search', 'grandmagazine' );
	$term = 'search';
}

$grandmagazine_hide_title = grandmagazine_get_hide_title();
if($grandmagazine_hide_title == 1)
{
	$page_show_title = 1;
}

if(empty($page_show_title))
{
	if(empty($page_tagline))
	{
		//Get current page tagline
		$page_tagline = get_post_meta($current_page_id, 'page_tagline', true);
	}
	
	//If on gallery post type page
	if(is_single() && $post->post_type == 'galleries')
	{
		$page_tagline = get_the_excerpt();
	}
	
	if(is_search())
	{
		$page_tagline = esc_html__('Search Results for ', 'grandmagazine' ).get_search_query().'&nbsp;'.esc_html__('keywords', 'grandmagazine' );
	}
	else if(is_category() OR is_tag())
	{
		$page_tagline = category_description();
	}

	$pp_page_bg = '';
	
	if(is_category())
	{
		//Get page featured image
		if(function_exists('z_taxonomy_image_url'))
	    {
	        $pp_page_bg = z_taxonomy_image_url();
	    }
	}
	elseif (is_page() && has_post_thumbnail($current_page_id, 'original'))
	{
		$image_id = get_post_thumbnail_id($current_page_id); 
	    $image_thumb = wp_get_attachment_image_src($image_id, 'original', true);
	    
	    if(isset($image_thumb[0]) && !empty($image_thumb[0]))
	    {
	    	$pp_page_bg = $image_thumb[0];
	    }
	}
    
    $grandmagazine_topbar = grandmagazine_get_topbar();
    $grandmagazine_screen_class = grandmagazine_get_screen_class();
?>
<div id="page_caption" class="<?php if(!empty($pp_page_bg)) { ?>hasbg parallax <?php } ?> <?php if(!empty($grandmagazine_topbar)) { ?>withtopbar<?php } ?> " <?php if(!empty($pp_page_bg)) { ?>style="background-image:url(<?php echo esc_url($pp_page_bg); ?>);"<?php } ?>>
	
	<?php if(!empty($pp_page_bg)) { ?>
		<div class="background_overlay"></div>
	<?php } ?>
	
	<div class="page_title_wrapper">
		
		<?php if(!empty($pp_page_bg)) { ?>
			<div class="standard_wrapper">
		<?php } ?>
		
		<?php if(is_author()) { ?>
		<div class="gravatar"><?php echo get_avatar( get_the_author_meta('email'), '80' ); ?></div>
		<?php } ?>
		
	    <div class="page_title_inner">
	    	<h1 <?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>class ="withtopbar"<?php } ?>><?php echo esc_html($page_title); ?></h1>
	    	<?php
	        	if(!empty($page_tagline))
	        	{
	        ?>
	        	<div class="page_tagline">
	        		<?php echo wp_kses_post($page_tagline); ?>
	        	</div>
	        <?php
	        	}
	        ?>
	    </div>
	    <a href="<?php echo esc_url(home_url('/')); ?>" class="return_home"><i class="fa fa-mail-reply"></i></a>
	    
	    <?php if(!empty($pp_page_bg)) { ?>
			</div>
		<?php } ?>
	</div>
	<br class="clear"/>
</div>

<?php
} //End if display title
?>

<!-- Begin content -->
<?php
	$grandmagazine_page_content_class = grandmagazine_get_page_content_class();
?>
<div id="page_content_wrapper" class="<?php if(!empty($pp_page_bg)) { ?>hasbg <?php } ?><?php if(!empty($pp_page_bg) && !empty($grandmagazine_topbar)) { ?>withtopbar <?php } ?><?php if(!empty($grandmagazine_page_content_class)) { echo esc_attr($grandmagazine_page_content_class); } ?>">