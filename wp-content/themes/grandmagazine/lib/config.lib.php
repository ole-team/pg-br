<?php
//Setup theme constant and default data
$theme_obj = wp_get_theme('grandmagazine');

define("GRANDMAGAZINE_THEMENAME", $theme_obj['Name']);
if (!defined('GRANDMAGAZINE_THEMEDEMO'))
{
	define("GRANDMAGAZINE_THEMEDEMO", false);
}
define("GRANDMAGAZINE_THEMEDEMOIG", 'kinfolklifestyle');
define("GRANDMAGAZINE_SHORTNAME", "pp");
define("GRANDMAGAZINE_THEMEVERSION", $theme_obj['Version']);
define("GRANDMAGAZINE_THEMEDATEFORMAT", get_option('date_format'));
define("GRANDMAGAZINE_THEMETIMEFORMAT", get_option('time_format'));
define("ENVATOITEMID", 19054317);

//Get default WP uploads folder
$wp_upload_arr = wp_upload_dir();
define("GRANDMAGAZINE_THEMEUPLOAD", $wp_upload_arr['basedir']."/".strtolower(sanitize_title(GRANDMAGAZINE_THEMENAME))."/");

if(!is_dir(GRANDMAGAZINE_THEMEUPLOAD))
{
	wp_mkdir_p(GRANDMAGAZINE_THEMEUPLOAD);
}

/**
*  Begin Global variables functions
*/

//Get default WordPress post variable
function grandmagazine_get_wp_post() {
	global $post;
	return $post;
}

//Get default WordPress file system variable
function grandmagazine_get_wp_filesystem() {
	require_once (ABSPATH . '/wp-admin/includes/file.php');
	WP_Filesystem();
	global $wp_filesystem;
	return $wp_filesystem;
}

//Get default WordPress wpdb variable
function grandmagazine_get_wpdb() {
	global $wpdb;
	return $wpdb;
}

//Get default WordPress wp_query variable
function grandmagazine_get_wp_query() {
	global $wp_query;
	return $wp_query;
}

//Get default WordPress customize variable
function grandmagazine_get_wp_customize() {
	global $wp_customize;
	return $wp_customize;
}

//Get default WordPress current screen variable
function grandmagazine_get_current_screen() {
	global $current_screen;
	return $current_screen;
}

//Get default WordPress paged variable
function grandmagazine_get_paged() {
	global $paged;
	return $paged;
}

//Get default WordPress registered widgets variable
function grandmagazine_get_registered_widget_controls() {
	global $wp_registered_widget_controls;
	return $wp_registered_widget_controls;
}

//Get default WordPress registered sidebars variable
function grandmagazine_get_registered_sidebars() {
	global $wp_registered_sidebars;
	return $wp_registered_sidebars;
}

//Get default Woocommerce variable
function grandmagazine_get_woocommerce() {
	global $woocommerce;
	return $woocommerce;
}

//Get all google font usages in customizer
function grandmagazine_get_google_fonts() {
	global $grandmagazine_google_fonts;
		
	//Define all google font usages in customizer
	$grandmagazine_google_fonts = array('tg_body_font', 'tg_header_font', 'tg_menu_font', 'tg_sidemenu_font', 'tg_sidebar_title_font', 'tg_button_font', 'tg_blog_title_font', 'tg_blog_date_font');
	
	return $grandmagazine_google_fonts;
}

//Get menu transparent variable
function grandmagazine_get_page_menu_transparent() {
	global $grandmagazine_page_menu_transparent;
	return $grandmagazine_page_menu_transparent;
}

//Set menu transparent variable
function grandmagazine_set_page_menu_transparent($new_value = '') {
	global $grandmagazine_page_menu_transparent;
	$grandmagazine_page_menu_transparent = $new_value;
}

//Get featured posts array
function grandmagazine_get_featured_posts() {
	global $grandmagazine_featured_posts;
	return $grandmagazine_featured_posts;
}

//Set featured posts array
function grandmagazine_set_featured_posts($new_value = '') {
	global $grandmagazine_featured_posts;
	$grandmagazine_featured_posts = $new_value;
}

//Get single post related posts array
function grandmagazine_get_single_related_posts() {
	global $grandmagazine_single_related_posts;
	return $grandmagazine_single_related_posts;
}

//Set single post related posts array
function grandmagazine_set_single_related_posts($new_value = '') {
	global $grandmagazine_single_related_posts;
	$grandmagazine_single_related_posts = $new_value;
}

//Get no header checker variable
function grandmagazine_get_is_no_header() {
	global $grandmagazine_is_no_header;
	return $grandmagazine_is_no_header;
}

//Get deafult theme screen CSS class
function grandmagazine_get_screen_class() {
	global $grandmagazine_screen_class;
	return $grandmagazine_screen_class;
}

//Set deafult theme screen CSS class
function grandmagazine_set_screen_class($new_value = '') {
	global $grandmagazine_screen_class;
	$grandmagazine_screen_class = $new_value;
}

//Get theme homepage style
function grandmagazine_get_homepage_style() {
	global $grandmagazine_homepage_style;
	return $grandmagazine_homepage_style;
}

//Set theme homepage style
function grandmagazine_set_homepage_style($new_value = '') {
	global $grandmagazine_homepage_style;
	$grandmagazine_homepage_style = $new_value;
}

//Get page gallery ID
function grandmagazine_get_page_gallery_id() {
	global $grandmagazine_page_gallery_id;
	return $grandmagazine_page_gallery_id;
}

//Get default theme options variable
function grandmagazine_get_options() {
	global $grandmagazine_options;
	return $grandmagazine_options;
}

//Set default theme options variable
function grandmagazine_set_options($new_value = '') {
	global $grandmagazine_options;
	$grandmagazine_options = $new_value;
}

//Get top bar setting
function grandmagazine_get_topbar() {
	global $grandmagazine_topbar;
	return $grandmagazine_topbar;
}

//Set top bar setting
function grandmagazine_set_topbar($new_value = '') {
	global $grandmagazine_topbar;
	$grandmagazine_topbar = $new_value;
}

//Get is hide title option
function grandmagazine_get_hide_title() {
	global $grandmagazine_hide_title;
	return $grandmagazine_hide_title;
}

//Set is hide title option
function grandmagazine_set_hide_title($new_value = '') {
	global $grandmagazine_hide_title;
	$grandmagazine_hide_title = $new_value;
}

//Get theme page content CSS class
function grandmagazine_get_page_content_class() {
	global $grandmagazine_page_content_class;
	return $grandmagazine_page_content_class;
}

//Set theme page content CSS class
function grandmagazine_set_page_content_class($new_value = '') {
	global $grandmagazine_page_content_class;
	$grandmagazine_page_content_class = $new_value;
}

//Get Kirki global variable
function grandmagazine_get_kirki() {
	global $kirki;
	return $kirki;
}

//Get admin theme global variable
function grandmagazine_get_wp_admin_css_colors() {
	global $_wp_admin_css_colors;
	return $_wp_admin_css_colors;
}

//Get theme plugins
function grandmagazine_get_plugins() {
	global $grandmagazine_tgm_plugins;
	return $grandmagazine_tgm_plugins;
}

//Set theme plugins
function grandmagazine_set_plugins($new_value = '') {
	global $grandmagazine_tgm_plugins;
	$grandmagazine_tgm_plugins = $new_value;
}

//Get blog layout
function grandmagazine_get_blog_layout() {
	global $grandmagazine_blog_layout;
	return $grandmagazine_blog_layout;
}

//Set blog layout
function grandmagazine_set_blog_layout($new_value = '') {
	global $grandmagazine_blog_layout;
	$grandmagazine_blog_layout = $new_value;
}

//Get page custom fields values
function grandmagazine_get_page_postmetas() {
	//Get all sidebars
	$theme_sidebar = array(
		'' => '',
		'Page Sidebar' => 'Page Sidebar', 
		'Contact Sidebar' => 'Contact Sidebar', 
		'Blog Sidebar' => 'Blog Sidebar',
	);
	
	$dynamic_sidebar = get_option('pp_sidebar');
	
	if(!empty($dynamic_sidebar))
	{
		foreach($dynamic_sidebar as $sidebar)
		{
			$theme_sidebar[$sidebar] = $sidebar;
		}
	}
	
	/*
		Get gallery list
	*/
	$args = array(
	    'numberposts' => -1,
	    'post_type' => array('galleries'),
	);
	
	$galleries_arr = get_posts($args);
	$galleries_select = array();
	$galleries_select['(Display Post Featured Image)'] = '';
	
	foreach($galleries_arr as $gallery)
	{
		$galleries_select[$gallery->ID] = $gallery->post_title;
	}
	
	/*
		Get page templates list
	*/
	if(function_exists('get_page_templates'))
	{
		$page_templates = get_page_templates();
		$page_templates_select = array();
		$page_key = 1;
		
		foreach ($page_templates as $template_name => $template_filename) 
		{
			$page_templates_select[$template_name] = get_template_directory_uri()."/functions/images/page/".basename($template_filename, '.php').".png";
			$page_key++;
		}
	}
	else
	{
		$page_templates_select = array();
	}
	
	/*
		Get all menus available
	*/
	$menus = get_terms('nav_menu');
	$menus_select = array(
		 '' => 'Default Menu'
	);
	foreach($menus as $each_menu)
	{
		$menus_select[$each_menu->slug] = $each_menu->name;
	}
	
	$grandmagazine_page_postmetas = array();
	$pp_menu_layout = get_option('pp_menu_layout');
		
	if($pp_menu_layout != 'leftmenu')
	{
	    $grandmagazine_page_postmetas[99] = array("section" => esc_html__('Page Menu', 'grandmagazine' ), "id" => "page_menu_transparent", "type" => "checkbox", "title" => esc_html__('Make Menu Transparent', 'grandmagazine' ), "description" => esc_html__('Check this option if you want to display main menu in transparent', 'grandmagazine' ));
	}
	
	$grandmagazine_page_postmetas_extended = 
		array (
			/*
				Begin Page custom fields
			*/
			array("section" => esc_html__('Page Title', 'grandmagazine' ), "id" => "page_show_title", "type" => "checkbox", "title" => esc_html__('Hide Page Title & Tagline', 'grandmagazine' ), "description" => esc_html__('Check this option if you want to hide page title and tagline.', 'grandmagazine' )),
		
			array("section" => esc_html__('Page Tagline', 'grandmagazine' ), "id" => "page_tagline", "type" => "textarea", "title" => esc_html__('Page Tagline (Optional)', 'grandmagazine' ), "description" => esc_html__('Enter page tagline. It will displays under page title (optional)', 'grandmagazine' )),
			
			array("section" => esc_html__('Select Sidebar (Optional)', 'grandmagazine' ), "id" => "page_sidebar", "type" => "select", "title" => esc_html__('Page Sidebar (Optional)', 'grandmagazine' ), "description" => esc_html__('Select this page\'s sidebar to display.', 'grandmagazine' ), "items" => $theme_sidebar),
		);
	
	
	$grandmagazine_page_postmetas = $grandmagazine_page_postmetas + $grandmagazine_page_postmetas_extended;
		
	return $grandmagazine_page_postmetas;
}
?>